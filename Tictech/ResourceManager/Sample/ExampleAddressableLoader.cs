using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using Tictech.ResourceManager;

namespace Tictech.ResourceManager.Sample
{
    public class ExampleAddressableLoader : MonoBehaviour
    {
        public Button loadAssetButton;
        public Button releaseAssetButton;
        public Button loadSceneButton;
        public AssetReference sceneReference;
        public AssetReference assetReference;
        public Transform parent;
        public int spawnCount;

        private int index;

        // Start is called before the first frame update
        void Start()
        {
            loadAssetButton.onClick.AddListener(Spawn);
            releaseAssetButton.onClick.AddListener(ReleaseAsset);
            loadSceneButton.onClick.AddListener(LoadScene);
        }

        private void Spawn()
        {
            for (int i = 0; i < spawnCount; i++)
            {
                ResourceManager.LoadAddressable(assetReference, () =>
                {
                    assetReference.InstantiateAsync(parent).Completed += (async) =>
                    {
                        async.Result.GetComponent<TMPro.TextMeshProUGUI>().text += " " + index;
                        index++;
                    };
                });
            }
        }

        private void LoadScene()
        {
            sceneReference.LoadSceneAsync(UnityEngine.SceneManagement.LoadSceneMode.Single);
        }

        private void ReleaseAsset()
        {
            ResourceManager.UnloadResource(assetReference.AssetGUID, true);
        }
    }
}
