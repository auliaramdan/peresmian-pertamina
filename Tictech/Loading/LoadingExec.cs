using System.Collections;
using UnityEngine;

public class LoadingExec : MonoBehaviour
{
    private static LoadingExec instance;

    public static Coroutine Run(IEnumerator routine)
    {
        if (instance == null)
        {
            instance = Instantiate(new GameObject()).AddComponent<LoadingExec>();
            instance.gameObject.name = "LoadingExec";
            DontDestroyOnLoad(instance);
            Debug.Log("Created loading exec");
        }
        return instance.StartCoroutine(routine);
    }
}
