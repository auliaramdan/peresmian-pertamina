using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Tictech.Loading
{
    using ResourceManager;
#if UNITY_EDITOR
    using TextDebugger;
#endif

    public class LoadingProcess : IDisposable
    {
        public string Id { get; private set; }
        public string Error { get; private set; }

        public long Size { get; private set; }
        public float ElapsedTime { get; private set; }

        public bool PendingCancel { get; private set; }
        public bool IsRunning { get; private set; }
        public bool IsFinished { get; private set; }

        public AsyncOperationHandle asyncHandle { get; private set; }
        public AsyncOperation asyncOperation { get; private set; }
        public UnityWebRequest webRequest { get; private set; }

        private IntPtr _handle;
        private bool _disposed = false;
        private float _progress;
        private string _message;

        public float Progress
        {
            get => _progress;
            set
            {
                _progress = value;
                onProgress?.Invoke(_progress);
                onProgressNormalized?.Invoke(NormalizedProgress);
                if (asyncOperation == null && NormalizedProgress >= 1)
                    Finish();
            }
        }

        public string Message
        {
            get => _message;
            set
            {
                _message = value;
                onMessageChanged?.Invoke(_message);
            }
        }

        public Action<float> onProgress;
        public Action<float> onProgressNormalized;
        public Action<string> onMessageChanged;
        public Action onFinished;

        IEnumerator SendRequest()
        {
#if UNITY_EDITOR
            string debugKey = "Loading " + Id;
#endif
            IsRunning = true;
            if (webRequest != null && webRequest.method == UnityWebRequest.kHttpVerbGET)
            {
#if UNITY_EDITOR
                Debugger.Track(debugKey, "Getting bytes length");
#endif
                Debug.Log("Getting bytes length for " + Id);
                LoadingExec.Run(ResourceManager.GetFilesSize((long sz) =>
                {
                    Size = sz;
#if UNITY_EDITOR
                    Debugger.Track(debugKey, "size: " + ResourceManager.LongToByteSizeFormat(Size));
#endif
                    Debug.Log(Id + " size: " + ResourceManager.LongToByteSizeFormat(Size));
                }, webRequest.url));
            }
            else if (asyncOperation == null)
            {
                if(asyncHandle.IsValid())
                {
                    LoadingExec.Run(ResourceManager.GetFilesSize((long sz) =>
                    {
                        Size = sz;
                    }, asyncHandle));
                }
                else
                {
                    Finish();
                    yield break;
                }
            }

            ElapsedTime = 0f;
            Progress = 0f;

            while (asyncOperation != null ? !asyncOperation.isDone : !asyncHandle.IsDone)
            {
                yield return new WaitForSeconds(LoadingManager.RefreshRate);
                Progress = webRequest == null ? (asyncHandle.IsValid() ? asyncHandle.GetDownloadStatus().DownloadedBytes : asyncOperation.progress) : webRequest.method == UnityWebRequest.kHttpVerbGET ? webRequest.downloadedBytes : webRequest.uploadedBytes;
                ElapsedTime += LoadingManager.RefreshRate;
            }

            if (webRequest != null && webRequest.result == UnityWebRequest.Result.ConnectionError)
            {
                Error = webRequest.error;
                Debug.LogError(webRequest.error);
            }
            Finish();
        }

        public float NormalizedProgress
        {
            get
            {
                return Progress / Size;
            }
        }

        public void Cancel()
        {
            PendingCancel = true;
        }

        public void Finish()
        {
            if (!IsFinished)
                LoadingExec.Run(Finishing());
        }

        private IEnumerator Finishing()
        {
            IsRunning = false;
            IsFinished = true;
            Debug.Log("Finished loading id: " + Id + ", ETA: " + ElapsedTime);
            yield return new WaitForEndOfFrame();
            onFinished?.Invoke();
            yield return new WaitForSeconds(.2f);
            LoadingManager.EndLoading(Id);
#if !UNITY_WEBGL
            Dispose();
#endif
        }

        #region CONSTRUCTOR
        public LoadingProcess(string key, long sz)
        {
            Id = key;
            Size = sz;
            Progress = 0f;
        }

        public LoadingProcess(string key, AsyncOperation async)
        {
            Id = key;
            Size = long.MaxValue;
            asyncOperation = async;
            LoadingExec.Run(SendRequest());
        }

        public LoadingProcess(string key, UnityWebRequest request)
        {
            Id = key;
            Size = long.MaxValue;
            webRequest = request;
            asyncOperation = webRequest.SendWebRequest();
            LoadingExec.Run(SendRequest());
        }

        public LoadingProcess(string key, AsyncOperationHandle handle)
        {
            Id = key;
            Size = long.MaxValue;
            asyncHandle = handle;
            LoadingExec.Run(SendRequest());
        }
        #endregion

        #region DISPOSE
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    // component.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                CloseHandle(_handle);
                _handle = IntPtr.Zero;

                // Note disposing has been done.
                _disposed = true;
            }
        }

        // Use interop to call the method necessary
        // to clean up the unmanaged resource.
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);
        #endregion
    }
}