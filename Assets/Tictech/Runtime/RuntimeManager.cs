using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.AddressableAssets;
using SandboxVRSeason2;
using SandboxVRSeason2.Framework;
using HurricaneVR.Framework.Core.Player;
using DG.Tweening;

namespace Tictech.LoadManager
{
    public class RuntimeManager : MonoBehaviour
    {
        private static RuntimeManager _instance;

        public static RuntimeManager Instance
        {
            get
            {
                return _instance;
            }
        }

        #region PUBLIC VARIABLES
        public AssetReference defaultScene;
        public Camera defaultCamera;
        public Sandbox_SubScenarioData currentSubScenario;
        public LoadingHelper loadingHelper;
        public Sandbox_AudioReference CommonAudioReference;
        #endregion

        #region STATIC VARIABLES
        public static bool IsReady => _instance != null;
        private static bool isLoadingScene = false;
        #endregion

        #region PRIVATE VARIABLES
        private static List<string> _activeScenes;
        private static List<AssetReference> _addressableScenes;
        private static string targetSceneName;
        #endregion

        #region CONSTANT VARIABLES
        private const string RUNTIME_SCENE = "Runtime";
        #endregion

        #region EVENTS
        public static Action<Scene, AsyncOperation> onSceneLoading;
        public static Action<AsyncOperationHandle> onAddressableSceneLoading;
        public static Action<Scene> onSceneLoaded;
        #endregion

        #region INITIALIZATION
        //[RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            _activeScenes = new List<string>();
            _addressableScenes = new List<AssetReference>();
            if (_instance == null)
            {
                _activeScenes.Add(SceneManager.GetActiveScene().name);
                SceneManager.LoadSceneAsync(RUNTIME_SCENE, LoadSceneMode.Additive);
            }
            else
            {
                print("kebudayaan");
                LoadScene(_instance.defaultScene, LoadSceneMode.Single);
            }
        }

        private void Awake()
        {
            _instance = this;
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }
        private void Start()
        {
            if (!currentSubScenario && FindObjectOfType<Sandbox_ScenarioManager>())
            {
                FindObjectOfType<Sandbox_ScenarioManager>().Initialize();
                loadingHelper.cg.interactable = true;
                loadingHelper.SetVisible(false);
            }

            if (FindObjectOfType<ScenarioTutorialButtonInteraction>())
            {
                loadingHelper.cg.interactable = true;
                loadingHelper.SetVisible(false);
            }
        }
        #endregion

        #region SCENE MANAGEMENT
        private void OnSceneLoaded(Scene scn, LoadSceneMode mode)
        {
            Debug.Log("Scene loaded: " + scn.name);
            defaultCamera.gameObject.SetActive(FindObjectsOfType(typeof(Camera), true).Length <= 1);
            onSceneLoaded?.Invoke(scn);

            isLoadingScene = false;
        }

        private void OnSceneUnloaded(Scene scn)
        {
            Debug.Log("Scene unloaded: " + scn.name);
            defaultCamera.gameObject.SetActive(FindObjectsOfType(typeof(Camera), true).Length <= 1);
        }

        public static void LoadScene(string sname, LoadSceneMode mode = LoadSceneMode.Single)
        {
            if (_activeScenes.Contains(sname) || isLoadingScene)
                return;

            isLoadingScene = true;
            LoadingExec.Run(_instance.LoadingScene(sname, mode));
        }

        public static void LoadScene(AssetReference scene, LoadSceneMode mode = LoadSceneMode.Single)
        {
            //if (_addressableScenes.Contains(scene) || isLoadingScene)
            if (isLoadingScene)
                return;
            isLoadingScene = true;
            LoadingExec.Run(_instance.LoadingScene(scene, mode));
        }

        public static void LoadScenario(Sandbox_SubScenarioData subScenario, bool isLinear)
        {
            LoadScene(subScenario.SubScenarioScene);

            onSceneLoaded += (x) =>
            {
                //print("Babi " + targetSceneName);
                //print("Set scene active success = " + SceneManager.SetActiveScene(SceneManager.GetSceneByName(targetSceneName)));
                SceneManager.SetActiveScene(x);
                //SceneManager.SetActiveScene(SceneManager.GetSceneByName(subScenario.SubScenarioScene));
                loadScenarioPrefab(subScenario.SubScenarioPrefab, isLinear);
                RuntimeManager.Instance.currentSubScenario = subScenario;
            };
        }

        private static void loadScenarioPrefab(AssetReference scenarioPrefab, bool isLinear)
        {
            onSceneLoaded = null;
            Sandbox_ScenarioManager scenario = null;
            scenarioPrefab.Spawn<Sandbox_ScenarioManager>(null, (result) =>
            {
                scenario = result;
                scenario.mode = isLinear ? ScenarioModeEnum.LINEAR : ScenarioModeEnum.SANDBOX;
                scenario.gameObject.SetActive(true);
                scenario.Initialize();
            });
        }

        private IEnumerator LoadingScene(string sname, LoadSceneMode mode)
        {
            if (mode == LoadSceneMode.Single)
                yield return UnloadAllScenes();
            yield return new WaitForEndOfFrame();

            var loading = SceneManager.LoadSceneAsync(sname, LoadSceneMode.Additive);
            onSceneLoading?.Invoke(SceneManager.GetSceneByName(sname), loading);
            yield return loading;
            _activeScenes.Add(sname);
        }

        private IEnumerator LoadingScene(AssetReference scene, LoadSceneMode mode)
        {
            ScreenFade(1, 1f);
            yield return new WaitForSeconds(1.1f);

            if (mode == LoadSceneMode.Single)
                yield return UnloadAllScenes();
            yield return new WaitForEndOfFrame();

            var loading = scene.LoadSceneAsync(LoadSceneMode.Additive);
            //targetSceneName = loading.Result.Scene.name;
            //print("Set scene active is = " + loading.Result.Scene.name);
            onAddressableSceneLoading?.Invoke(loading);
            yield return loading;
            _addressableScenes.Add(scene);
            yield return 0;
            //SceneManager.SetActiveScene(loading.Result.Scene);
            //targetSceneName = loading.Result.Scene.name;
            //print("Set scene active success = " + SceneManager.SetActiveScene(loading.Result.Scene));
            //print("Set scene active is = " + loading.Result.Scene.name);
        }

        private IEnumerator UnloadAllScenes()
        {
            foreach (var e in _activeScenes)
            {
                yield return SceneManager.UnloadSceneAsync(e);
            }
            _activeScenes.Clear();

            foreach (var e in _addressableScenes)
            {
                yield return e.UnLoadScene();
            }
            _addressableScenes.Clear();

            ResourceManager.UnloadAllResources();
        }
        #endregion

        #region ASSET MANAGEMENT
        public static T LoadAsset<T>(string path)
        {
            Debug.Log("Loading asset of type " + typeof(T) + " at: " + path);
            T asset = default;
            return asset;
        }
        #endregion

        #region UTILITY
        public void ScreenFade(float alpha, float time)
        {
            var finder = FindObjectOfType<HVRGlobalFadeFinder>();
            if (finder)
            {
                var ScreenFader = finder.gameObject.GetComponent<HVRCanvasFade>();
                if (ScreenFader)
                    ScreenFader.CanvasGroup.DOFade(alpha, time);
            }
        }

        #endregion
    }

}