using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tictech.LoadManager
{
    public class LoadingExec : MonoBehaviour
    {
        private static LoadingExec instance;

        public static Coroutine Run(IEnumerator routine)
        {
            if (instance == null)
            {
                instance = new GameObject().AddComponent<LoadingExec>();
                instance.gameObject.name = "LoadingExec";
                if (RuntimeManager.IsReady)
                    instance.transform.parent = RuntimeManager.Instance.transform;
                else
                    DontDestroyOnLoad(instance);
                Debug.Log("Created loading exec");
            }
            return instance.StartCoroutine(routine);
        }
    }
}
