using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

namespace Tictech.LoadManager
{
    public class Debugger : MonoBehaviour
    {
        [System.Serializable]
        public class TrackedDebug
        {
            public string Key { get; private set; }
            public TextMeshProUGUI debug;
            private float lifeTime = 8f;

            public TrackedDebug(string key, TextMeshProUGUI text, string message)
            {
                this.Key = key;
                debug = text;
                this.Update(message);
                LoadingExec.Run(DebugTracker());
            }

            public void Update(string message)
            {
                debug.text = Key + " -> " + message;
                debug.color = Color.white;
                debug.fontSize = defaultFontSize;
                lifeTime = 8f;
                AddLog(Key, message);
            }

            private IEnumerator DebugTracker()
            {
                while (lifeTime > 0f)
                {
                    yield return new WaitForSeconds(1f);
                    lifeTime -= 1f;
                }
                Destroy(debug.gameObject);
                Delete(Key);
            }
        }

        public static Debugger Instance;

        public Transform textContainer;
        public GameObject textPrefab;
        public bool logCallback;
        public bool editorOnly;
        public bool saveLog;

        private Dictionary<string, TrackedDebug> _trackedDebug;
        private Dictionary<string, List<string>> _debugLog;
        private static float defaultFontSize, warningFontSize, errorFontSize;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                _trackedDebug = new Dictionary<string, TrackedDebug>();
                _debugLog = new Dictionary<string, List<string>>();
                var sample = Instantiate(textPrefab);
                sample.name = "debugger sample";
                defaultFontSize = sample.GetComponentInChildren<TextMeshProUGUI>().fontSize;
                warningFontSize = defaultFontSize * 1.05f;
                errorFontSize = warningFontSize * 1.05f;
                if(logCallback)
                {
                    Application.logMessageReceived += (log, stack, type) =>
                    {
                        switch (type)
                        {
                            case LogType.Error:
                                TrackError(log, stack);
                                break;
                            case LogType.Assert:
                                TrackError(log, stack);
                                break;
                            case LogType.Warning:
                                TrackWarning(log, stack);
                                break;
                            case LogType.Log:
                                Track(log, stack);
                                break;
                            case LogType.Exception:
                                TrackError(log, stack);
                                break;
                        }
                    };
                }
                Destroy(sample);
                //DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void OnApplicationQuit()
        {
            if (!saveLog || !ShouldTrack)
                return;

            var entries = _debugLog.Select(d =>
                string.Format("    \"{0}\": [\r\n        {1}\r\n    ]", d.Key, string.Join(",\r\n        ", d.Value)));
            string json = "{\r\n" + string.Join(",\r\n", entries) + "\r\n}";

            string path = Application.persistentDataPath + "/textDebuggerLog-" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".txt";
            System.IO.File.WriteAllText(path, json);
            Debug.Log("TextDebugger log file saved at: " + path);
        }

        public static void TrackWarning(string key, string value)
        {
            if (Instance == null || !ShouldTrack)
                return;

            Track(key, value);
            Instance._trackedDebug[key].debug.color = Color.yellow;
            Instance._trackedDebug[key].debug.fontSize = warningFontSize;
        }

        public static void TrackError(string key, string value)
        {
            if (Instance == null || !ShouldTrack)
                return;

            Track(key, value);
            Instance._trackedDebug[key].debug.color = Color.red;
            Instance._trackedDebug[key].debug.fontSize = errorFontSize;
            Instance._trackedDebug[key].debug.fontStyle = FontStyles.Bold;
        }

        public static void Track(string key, string value)
        {
            if (Instance == null || !ShouldTrack)
                return;

            if (Instance._trackedDebug.ContainsKey(key))
                Instance._trackedDebug[key].Update(value);
            else
            {
                if (!Instance._debugLog.ContainsKey(key))
                    Instance._debugLog.Add(key, new List<string>());
                Instance._trackedDebug.Add(key, new TrackedDebug(key, Instantiate(Instance.textPrefab, Instance.textContainer).GetComponent<TextMeshProUGUI>(), value));
            }
        }

        public static void Delete(string key)
        {
            if (Instance._trackedDebug.ContainsKey(key))
                Instance._trackedDebug.Remove(key);
        }

        public static void AddLog(string key, string message)
        {
            if (!Instance.saveLog)
                return;

            Instance._debugLog[key].Add("[" + DateTime.Now.ToString("HH:mm:ss") + "] " + message);
        }

        public static bool ShouldTrack
        {
            get
            {
                return !(Instance.editorOnly && Application.platform != RuntimePlatform.WindowsEditor);
            }
        }
    }
}
