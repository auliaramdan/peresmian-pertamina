﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Tictech.LoadManager
{
    public class ResourceManager
    {
        public enum ContentType
        {
            Texture,
            Audio,
            //Video,
            AssetBundle,
            Addressables
        }

        public class ResourceData : IDisposable
        {
            public string url;
            public ContentType type;
            public AssetReference assetReference;
            public Action<float> onProgressNormalized;
            public Action<object> onComplete;
            public LoadingProcess process;
            public object data;

            public ulong bytesSize;

            public bool isLoaded;
            public string[] adds;

            private bool _released;
            private IntPtr _handle;
            private bool _disposed = false;

            public ResourceData(AssetReference asset, Action onComplete)
            {
                url = asset.AssetGUID;
                assetReference = asset;
                type = ContentType.Addressables;
                isLoaded = asset.OperationHandle.IsValid();
                if (!isLoaded && LoadingManager.StartLoading(url, assetReference.LoadAssetAsync<GameObject>()))
                {
                    process = LoadingManager.GetLoading(url);
                    process.onFinished += onComplete;
                    process.onFinished += () =>
                    {
                        Debug.Log("Asset is loaded: " + asset.Asset.name);
                        isLoaded = true;
                    };
                }
                else if (asset.OperationHandle.IsDone)
                {
                    onComplete?.Invoke();
                }
                else
                {
                    asset.OperationHandle.Completed += (async) =>
                    {
                        onComplete?.Invoke();
                    };
                }
            }

            public ResourceData(string u, ContentType ct, Action<object> oc, params string[] ad)
            {
                url = u;
                type = ct;
                onComplete = oc;
                bytesSize = 0;
                isLoaded = false;
                adds = ad;
            }

            public void Cancel()
            {
                onComplete = null;
                if (process != null)
                    process.Cancel();
            }

            public void Release()
            {
                if (_released)
                    return;

                Dispose();
                Debugger.Track("Released cache", url);
            }

            #region DISPOSE
            // Implement IDisposable.
            // Do not make this method virtual.
            // A derived class should not be able to override this method.
            public void Dispose()
            {
                Dispose(true);
                // This object will be cleaned up by the Dispose method.
                // Therefore, you should call GC.SupressFinalize to
                // take this object off the finalization queue
                // and prevent finalization code for this object
                // from executing a second time.
                GC.SuppressFinalize(this);
            }

            // Dispose(bool disposing) executes in two distinct scenarios.
            // If disposing equals true, the method has been called directly
            // or indirectly by a user's code. Managed and unmanaged resources
            // can be disposed.
            // If disposing equals false, the method has been called by the
            // runtime from inside the finalizer and you should not reference
            // other objects. Only unmanaged resources can be disposed.
            protected virtual void Dispose(bool disposing)
            {
                // Check to see if Dispose has already been called.
                if (!this._disposed)
                {
                    if (disposing)
                    {
                        if (assetReference != null && isLoaded)
                        {
                            Debug.Log("Asset released: " + assetReference.Asset.name);
                            assetReference.ReleaseAsset();
                        }
                        _released = true;
                        data = null;
                        //LoadedResource.Remove(url);
                    }

                    // Call the appropriate methods to clean up
                    // unmanaged resources here.
                    // If disposing is false,
                    // only the following code is executed.
                    CloseHandle(_handle);
                    _handle = IntPtr.Zero;

                    // Note disposing has been done.
                    _disposed = true;
                }
            }

            // Use interop to call the method necessary
            // to clean up the unmanaged resource.
            [System.Runtime.InteropServices.DllImport("Kernel32")]
            private extern static Boolean CloseHandle(IntPtr handle);
            #endregion
        }

        public static Dictionary<string, ResourceData> LoadedResource { get; private set; }
        public static Dictionary<string, ResourceData> LoadQueue { get; private set; }
        public static int LoadCount { get; private set; }
        public static int LoadLimit = 1;
        public static ulong CacheLimit = 1;

        private static bool _isReleasingCache;

        [RuntimeInitializeOnLoadMethod]
        private static void Prepare()
        {
            if (LoadedResource != null)
                return;

            LoadedResource = new Dictionary<string, ResourceData>();
            LoadQueue = new Dictionary<string, ResourceData>();
            Debug.Log("Initialized Resource Manager");
        }

        public static float GetLoadedResourceSize()
        {
            ulong size = 0;
            foreach (var e in LoadedResource.Values)
                size += e.bytesSize;
            return size;
        }

        public static ResourceData LoadAddressable(AssetReference asset, Action onComplete)
        {
            if (asset == null)
            {
                Debug.LogWarning("Asset reference can't be null.");
                return null;
            }

            string key = asset.AssetGUID;
            if (LoadedResource.ContainsKey(key) && LoadedResource[key].isLoaded)
            {
                onComplete?.Invoke();
                return null;
            }
            QueueRequest(new ResourceData(asset, onComplete));

            return LoadQueue[key];
        }

        public static ResourceData LoadResource(string url, ContentType type, Action<object> onComplete, params string[] adds)
        {
            if (string.IsNullOrEmpty(url))
            {
                Debug.Log("Url can't be null.");
                return null;
            }

            if (LoadedResource.ContainsKey(url) && LoadedResource[url].isLoaded)
            {
                onComplete?.Invoke(LoadedResource[url].data);
                return null;
            }
            QueueRequest(new ResourceData(url, type, onComplete, adds));

            return LoadQueue[url];
        }

        private static void QueueRequest(ResourceData request)
        {
            string key = request.url;

            if (!LoadQueue.ContainsKey(key))
                LoadQueue.Add(key, request);
            else
                LoadingExec.Run(WaitRequest(LoadQueue[key], request.onComplete));

            if (LoadCount < LoadLimit)
                LoadingExec.Run(DownloadContent());
        }

        public static void CancelLoad(string key)
        {
            if (LoadQueue.ContainsKey(key))
                LoadQueue[key].Cancel();
        }

        public static void CancelAllLoad()
        {
            foreach (var e in LoadQueue.Values)
                e.Cancel();
        }

        private static IEnumerator WaitRequest(ResourceData request, Action<object> onComplete)
        {
            while (!request.isLoaded)
                yield return new WaitForSeconds(.4f);
            onComplete?.Invoke(request.data);
        }

        private static IEnumerator DownloadContent()
        {
            yield return new WaitForEndOfFrame();
            LoadCount++;
            var request = LoadQueue.Values.Where(e => !e.isLoaded).FirstOrDefault();
            while (request != null)
            {
                if (request.type == ContentType.Addressables)
                {
                    yield return LoadWithAddressable(request);
                }
                else
                {
                    yield return LoadWithNetworking(request);
                }

                //Debug.Log("Request completed: " + request.url);
                request = LoadQueue.Values.Where(e => !e.isLoaded).FirstOrDefault();
            }
            LoadCount--;
        }

        private static IEnumerator LoadWithAddressable(ResourceData request)
        {
            if (request.isLoaded)
                yield break;

            while (!request.isLoaded)
            {
                yield return new WaitForSeconds(.1f);
                if (request.process.PendingCancel)
                    break;
            }

            LoadQueue.Remove(request.url);
            if (request.process.PendingCancel)
            {
                yield break;
            }

            if (LoadedResource.ContainsKey(request.url))
                LoadedResource[request.url] = request;
            else
                LoadedResource.Add(request.url, request);
        }

        private static IEnumerator LoadWithNetworking(ResourceData request)
        {
            if (request.process.PendingCancel)
            {
                LoadQueue.Remove(request.url);
                yield break;
            }

            yield return GetFileSize(request.url,
                        (size) =>
                        {
                            request.bytesSize = (ulong)size;
                            Debug.Log("Downloading content: " + LongToByteSizeFormat(size));
                        });

            UnityWebRequest getData = UnityWebRequest.Get("");

            switch (request.type)
            {
                case ContentType.Texture:
                    getData = UnityWebRequestTexture.GetTexture(request.url);
                    break;
                case ContentType.Audio:
                    getData = UnityWebRequestMultimedia.GetAudioClip(request.url, AudioType.MPEG);
                    break;
                /*case ContentType.Video:
                    break;*/
                case ContentType.AssetBundle:
                    Debug.Log("Loading assetbundle, cached: " + Caching.IsVersionCached(request.url, Hash128.Parse(request.adds[0])) + ", url: " + request.url);
                    getData = UnityWebRequestAssetBundle.GetAssetBundle(request.url, Hash128.Parse(request.adds[0]), uint.Parse(request.adds[1]));
                    break;
            }

            Debug.Log("Getting resource from: " + getData.url);
            LoadingManager.StartLoading(request.url, getData);
            request.process = LoadingManager.GetLoading(request.url);
            while (!request.process.IsFinished)
            {
                yield return new WaitForSeconds(.1f);
                if (request.process.PendingCancel)
                    break;
            }

            LoadQueue.Remove(request.url);
            if (request.process.PendingCancel)
            {
                yield break;
            }

            if (getData.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.LogError(getData.error);
                request.onComplete?.Invoke(null);
                request.onComplete = null;
            }
            else
            {
                switch (request.type)
                {
                    case ContentType.Texture:
                        request.data = DownloadHandlerTexture.GetContent(getData);
                        break;
                    case ContentType.Audio:
                        request.data = DownloadHandlerAudioClip.GetContent(getData);
                        break;
                    case ContentType.AssetBundle:
                        request.data = DownloadHandlerAssetBundle.GetContent(getData);
                        break;
                }
                if (LoadedResource.ContainsKey(request.url))
                    LoadedResource[request.url] = request;
                else
                    LoadedResource.Add(request.url, request);

                request.isLoaded = true;
                request.onComplete?.Invoke(LoadedResource[request.url].data);
                request.onComplete = null;
                Debugger.Track("Loaded resources", (GetLoadedResourceSize() / 1000) + " KB");
                if (GetLoadedResourceSize() > CacheLimit && !_isReleasingCache)
                    LoadingExec.Run(ReleaseCache());
            }
        }

        public static bool UnloadResource(string key, bool release)
        {
            if (!LoadedResource.ContainsKey(key))
                return false;

            LoadedResource[key].data = null;
            LoadedResource[key].isLoaded = false;
            LoadedResource[key].Release();
            if (release)
                LoadedResource.Remove(key);
            return true;
        }

        public static void UnloadAllResources()
        {
            foreach (var key in LoadedResource.Keys)
                UnloadResource(key, false);

            LoadedResource.Clear();
        }

        public static IEnumerator GetFilesSize(Action<long> result, params string[] urls)
        {
            long totalSize = 0;
            foreach (var url in urls)
            {
                yield return GetFileSize(url, (long size) =>
                {
                    totalSize += size;
                });
            }
            result(totalSize);
        }

        public static IEnumerator GetFilesSize(Action<long> result, AsyncOperationHandle handle)
        {
            yield return new WaitForEndOfFrame();
            while (handle.IsValid() && handle.GetDownloadStatus().TotalBytes <= 0)
            {
                yield return new WaitForSeconds(LoadingManager.RefreshRate);
            }
            result(handle.IsValid() ? handle.GetDownloadStatus().TotalBytes : 0);
        }

        private static IEnumerator GetFileSize(string url, Action<long> result)
        {
            UnityWebRequest uwr = UnityWebRequest.Head(url);
            var uwrRequest = uwr.SendWebRequest();
            while (!uwrRequest.isDone)
                yield return new WaitForSeconds(LoadingManager.RefreshRate);

            string size = uwr.GetResponseHeader("Content-Length");

            if (uwr.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.LogError("Error While Getting Length: " + uwr.error);
                result?.Invoke(-1);
            }
            else
            {
                result?.Invoke(Convert.ToInt64(size));
            }
        }

        private static IEnumerator ReleaseCache()
        {
            _isReleasingCache = true;
            while (GetLoadedResourceSize() > CacheLimit)
            {
                LoadedResource.Values.First().Release();
                yield return new WaitForEndOfFrame();
            }
            _isReleasingCache = false;
        }

        #region UTILITIES
        public static bool IsRequestError(UnityWebRequest request)
        {
            return request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.DataProcessingError || request.result == UnityWebRequest.Result.ProtocolError;
        }

        public static string LongToByteSizeFormat(long size)
        {
            if (size < 1000)
                return size + " B";
            else if (size < 1000000)
                return (size / 1000) + " KB";
            else if (size < 1000000000)
                return (size / 1000000f).ToString("F2") + " MB";
            else if (size < 1000000000000)
                return (size / 1000000000f).ToString("F2") + " GB";
            else if (size < 1000000000000000)
                return (size / 1000000000000f).ToString("F2") + " TB";
            else if (size < 1000000000000000000)
                return (size / 1000000000000000f).ToString("F2") + " PB";
            else
                return (size / 1000000000000000000f).ToString("F2") + " EB";
        }

        public static string TimespanStringFormat(TimeSpan span)
        {
            return TimeDigit(span.TotalHours) + (int)span.TotalHours + " : " + TimeDigit(span.Minutes) + span.Minutes + " : " + TimeDigit(span.Seconds) + span.Seconds;
        }

        public static DateTime StringToDateTime(string dt)
        {
            return DateTime.Parse(dt);
        }

        private static string TimeDigit(double val)
        {
            return val >= 10 ? "" : "0";
        }
        #endregion
    }
}