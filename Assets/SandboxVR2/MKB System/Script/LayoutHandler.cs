﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct LayoutConfig
{
    public string configName;
    public LayoutSetting layoutSetting;
}

public class LayoutHandler : MonoBehaviour
{
    [Header("Active Layout Setting")]
    public LayoutSetting activeLayoutSetting;

    [Header("Layout Settings")]
    public List<LayoutSetting> layoutSettings;

    /*public LayoutSetting horizontalLayoutSetting;
    public LayoutSetting verticalLayoutSetting;*/

    public List<string> ignoredTags;

    private bool isLandscape;

    private void SetUI()
    {
        //If current screen mode is landscape and previous screen mode was portrait
        if (Screen.width > Screen.height)
        {
            if (!isLandscape)
            {
                isLandscape = true;
                LoadLayoutSetting(layoutSettings[0]);
            }
        }
        //If current screen mode is portrait and previous screen mode was landscape
        else
        {
            if (isLandscape)
            {
                isLandscape = false;
                LoadLayoutSetting(layoutSettings[1]);
            }
        }
    }

    private void Start()
    {
        LoadLayoutSetting(activeLayoutSetting);
    }

    public void LoadLayoutSetting(int i)
    {
        LoadLayoutSetting(layoutSettings[i]);
    }

    public void LoadLayoutSetting(string i)
    {
        LoadLayoutSetting(layoutSettings.Find(x => x.name == i));
    }

    public void LoadLayoutSetting(LayoutSetting layoutSetting)
    {
        RectTransform[] rectTransforms = transform.GetComponentsInChildren<RectTransform>(true);
        List<RectTransform> nonGridRectTransforms = new List<RectTransform>();
        List<UIObjectProperties> setting = layoutSetting._UIObjectProperties;

        for (int i = 0; i < rectTransforms.Length; i++)
        {
            if (!ignoredTags.Contains(rectTransforms[i].tag))
            {
                nonGridRectTransforms.Add(rectTransforms[i]);
            }
        }

        if (setting.Count != nonGridRectTransforms.Count)
        {
            Debug.LogError("There is a difference in the number of unignored childs and the number of RectTransform configurations!\nAdjust the number of your child objects or save current layout settings first, then load!");
            return;
        }

        //Debug.Log("Initialize layout setting");

        for (int i = 0; i < nonGridRectTransforms.Count; i++)
        {
            if (nonGridRectTransforms[i].GetComponent<Canvas>())
                nonGridRectTransforms[i].GetComponent<Canvas>().renderMode = setting[i].renderMode;
            nonGridRectTransforms[i].pivot = setting[i].pivot;
            nonGridRectTransforms[i].offsetMax = setting[i].offsetMax;
            nonGridRectTransforms[i].offsetMin = setting[i].offsetMin;
            nonGridRectTransforms[i].anchorMax = setting[i].anchorMax;
            nonGridRectTransforms[i].anchorMin = setting[i].anchorMin;
            nonGridRectTransforms[i].localScale = setting[i].scale;
        }
    }

    private void Awake()
    {
        isLandscape = !(Screen.width > Screen.height);
    }

    private void LateUpdate()
    {
        //SetUI();
    }
}
