﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct UIObjectProperties
{
    public string name;
    public Rect rectTransformAnchoredPos;
    public RenderMode renderMode;

    public Vector2 offsetMin;
    public Vector2 offsetMax;
    public Vector2 anchorMin;
    public Vector2 anchorMax;
    public Vector2 pivot;
    public Vector3 scale;
}

[CreateAssetMenu(fileName = "LayoutSetting", menuName = "UI/LayoutSetting")]
public class LayoutSetting : ScriptableObject
{
    public List<UIObjectProperties> _UIObjectProperties = new List<UIObjectProperties>();

    public void SaveLayoutSetting(List<RectTransform> rectTransform)
    {
        Debug.Log("Getting properties for " + this.name + " scriptable object");

        _UIObjectProperties.Clear();

        UIObjectProperties newUIObjectPropercties = new UIObjectProperties();

        for (int i = 0; i < rectTransform.Count; i++)
        {

            newUIObjectPropercties.name = rectTransform[i].name + "-" + rectTransform[i].GetInstanceID();
            newUIObjectPropercties.rectTransformAnchoredPos = rectTransform[i].rect;

            if(rectTransform[i].GetComponent<Canvas>())
                newUIObjectPropercties.renderMode = rectTransform[i].GetComponent<Canvas>().renderMode;
            newUIObjectPropercties.offsetMax = rectTransform[i].offsetMax;
            newUIObjectPropercties.offsetMin = rectTransform[i].offsetMin;
            newUIObjectPropercties.anchorMax = rectTransform[i].anchorMax;
            newUIObjectPropercties.anchorMin = rectTransform[i].anchorMin;
            newUIObjectPropercties.pivot = rectTransform[i].pivot;
            newUIObjectPropercties.scale = rectTransform[i].localScale;

            _UIObjectProperties.Add(newUIObjectPropercties);

        }
    }
}
