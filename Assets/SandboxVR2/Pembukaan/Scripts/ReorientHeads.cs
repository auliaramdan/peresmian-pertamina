using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HurricaneVR.Framework.Core.Player;

public class ReorientHeads : MonoBehaviour
{
    [SerializeField] private GameObject reference;
    [SerializeField] private HVRPlayerController player; 

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.1f);
        player.transform.rotation = Quaternion.Euler(0,180,0);
        //player.transform.localPosition = new Vector3(0,player.transform.localPosition.y, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
