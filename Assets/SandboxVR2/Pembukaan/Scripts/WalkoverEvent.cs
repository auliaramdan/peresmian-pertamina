using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class WalkoverEvent : MonoBehaviour
{
    [SerializeField] private UnityEvent onWalkedIn;
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;
        
        onWalkedIn?.Invoke();
    }

    public void PrepareQuitScene(string sceneName)
    {
        StartCoroutine(QuitScene(sceneName));
    }

    private IEnumerator QuitScene(string newScene)
    {
        //yield return new WaitForSeconds(5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(newScene, LoadSceneMode.Single);
        Debug.Log(newScene);
        yield return new WaitWhile(() => async.progress >= 88);

        async.allowSceneActivation = true;
    }
}
