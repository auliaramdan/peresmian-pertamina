using System;
using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.Shared.HandPoser;
using UnityEngine;
using UnityEngine.Events;

public class Scanner : MonoBehaviour
{
    [SerializeField] private string handTag;
    [SerializeField] private float timeTaken = 1;
    
    [Space]
    
    [SerializeField] private GameObject leftHandScanObject, rightHandScanObject;
    
    [Space]
    
    [SerializeField] private UnityEvent onComplete;
    [SerializeField] private GameObject scannerGO;

    #if UNITY_EDITOR

    [SerializeField] private bool debugMode = false;

    #endif

    private bool _isScanning = false, _isComplete = false;
    private bool _leftIn = false, _rightIn = false;
    private float _scanTimeElapsed = 0f;
    private Material _scannerMaterial;
    private static readonly int Speed = Shader.PropertyToID("_Speed");
    private static readonly int Transparency = Shader.PropertyToID("_Transparency");

    private void Start()
    {
        _scannerMaterial = scannerGO.GetComponent<Renderer>().material;
        _scannerMaterial.SetFloat(Transparency, 0);
    }

    private void Update()
    {
        //_scannerMaterial.SetFloat(Speed, Mathf.Lerp(0, 1, _scanTimeElapsed/timeTaken));
        
        if (!_isScanning || _isComplete)
            return;
        
        _scanTimeElapsed += Time.deltaTime;
        
        _scannerMaterial.SetFloat(Transparency, Mathf.Lerp(0, 1, _scanTimeElapsed/timeTaken));

        if (_scanTimeElapsed >= timeTaken)
        {
            onComplete?.Invoke();
            _isComplete = true;
            rightHandScanObject.SetActive(false);
            leftHandScanObject.SetActive(false);
            _scannerMaterial.SetFloat(Transparency, 0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isComplete || !other.CompareTag(handTag))
            return;
        
        if (CheckIsLeftHand(other.transform.parent.gameObject))
        {
            _leftIn = true;
            leftHandScanObject.SetActive(true);
        }
        else
        {
            _rightIn = true;
            rightHandScanObject.SetActive(true);
        }
        
        if (!_isScanning)
            _isScanning = true;
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (_isComplete || !other.CompareTag(handTag))
            return;
        
        if (CheckIsLeftHand(other.transform.parent.gameObject))
        {
            leftHandScanObject.SetActive(false);
            _leftIn = false;
        }
        else
        {
            _rightIn = false;
            rightHandScanObject.SetActive(false);
        }
        
        if (!other.CompareTag(handTag) || !_isScanning || _leftIn || _rightIn)
            return;

        _isScanning = false;
        _scanTimeElapsed = 0f;
        _scannerMaterial.SetFloat(Transparency, 0);
    }

    private bool CheckIsLeftHand(GameObject other)
    {
        #if UNITY_EDITOR
            if(debugMode)
                return true;
        #endif

        HVRPosableHand[] poseHands = other.transform.GetComponentsInChildren<HVRPosableHand>();

        bool isLeft = false;

        foreach (var hand in poseHands)
        {
            isLeft = hand.IsLeft;
        }

        return isLeft;
    }
}
