﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerExperienceCityLoop : MonoBehaviour
{
    [SerializeField] private float loopSpeed = 1 ,topSpeed = 10;
    [SerializeField] private float minimalZValue = -20;
    [SerializeField] private float newZValue = 148;
    
    [SerializeField] private GameObject[] cityLoop;

    public int loopMax, maxLoop;
    
    private Vector3 _newPositionValue = new Vector3();
    
    // Start is called before the first frame update
    void Start()
    {
        _newPositionValue = cityLoop[0].transform.position;
        //newZValue = Vector3.Distance(cityLoop[0].transform.position, cityLoop[1].transform.position) * 2 * Mathf.Sign(cityLoop[0].transform.position.z - cityLoop[1].transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (loopMax >= maxLoop)
            return;
        
        for (var i = 0; i < cityLoop.Length; i++)
        {
            _newPositionValue.z = cityLoop[i].transform.position.z + loopSpeed * Time.deltaTime;
            cityLoop[i].transform.position = _newPositionValue;
            
            if(minimalZValue < 0)
            {
                if (cityLoop[i].transform.position.z <= minimalZValue)
                {
                    Loop(cityLoop[i]);
                }
            }
            else
            {
                if (cityLoop[i].transform.position.z >= minimalZValue)
                {
                    Loop(cityLoop[i]);
                }
            }

            if (i == cityLoop.Length - 1)
            {
                loopMax++;
            }
        }
    }

    public void AlterSpeed(float increment, float second)
    {
        StopAllCoroutines();
        StartCoroutine(ChangeSpeed(increment, second));
    }
    
    public void AlterSpeedInOneSecond(float increment)
    {
        AlterSpeed(increment, 1);
    }
    
    public void ReachTopSpeedInSeconds(float seconds)
    {
        StopAllCoroutines();
        StartCoroutine(ChangeSpeed(topSpeed, seconds));
    }
    
    public void AlterSpeedInTwoSeconds(float increment)
    {
        AlterSpeed(increment, 2);
    }

    public void SetSpeed(float speed)
    {
        StopAllCoroutines();
        loopSpeed = speed;
    }

    private void Loop(GameObject loopedObject)
    {
        _newPositionValue.z = loopedObject.transform.position.z + newZValue;
        loopedObject.transform.position = _newPositionValue;
    }

    IEnumerator ChangeSpeed(float goal, float second)
    {
        float originalSpeed = loopSpeed;
        float progress = 0f;

        while (progress <= 99f)
        {
            progress += Time.deltaTime * second;
            loopSpeed = Mathf.Lerp(originalSpeed, goal, progress);
            yield return null;
        }
    }
}
