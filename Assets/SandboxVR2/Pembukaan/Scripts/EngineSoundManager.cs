using System;
using System.Collections;
using System.Collections.Generic;
using Hellmade.Sound;
using UnityEngine;

public class EngineSoundManager : MonoBehaviour
{
    [SerializeField] private float volume = 1;
    
    [SerializeField] private AudioClip buzzerClip;
    [SerializeField] private AudioClip openDoorClip;
    [SerializeField] private AudioClip bgmClipBeforeOpen;
    [SerializeField] private AudioClip bgmClipAfterOpen;
    [SerializeField] private AudioClip swooshClip;
    
    private Audio _buzzerAudio;
    private Audio _openDoorAudio;
    private Audio _bgmBeforeOpenAudio;
    private Audio _bgmAfterOpenAudio;
    private Audio _swooshAudio;

    private int _buzzerClipID;
    private int _openDoorID;
    private int _bgmBeforeOpenID;
    private int _bgmAfterOpenID;
    private int _swooshID;
    
    // Start is called before the first frame update
    void Start()
    {
        if (_buzzerAudio == null)
        {
            _buzzerClipID = EazySoundManager.PlaySound(buzzerClip, volume, false, gameObject.transform);
            _buzzerAudio = EazySoundManager.GetAudio(_buzzerClipID);
            _buzzerAudio.SpatialBlend = 0f;
        }
        
        if (_openDoorAudio == null)
        {
            _openDoorID = EazySoundManager.PlaySound(openDoorClip, volume, false, gameObject.transform);
            _openDoorAudio = EazySoundManager.GetAudio(_openDoorID);
            _openDoorAudio.SpatialBlend = 0f;
        }

        if (_bgmBeforeOpenAudio == null)
        {
            _bgmBeforeOpenID = EazySoundManager.PlaySound(bgmClipBeforeOpen, volume, true, gameObject.transform);
            _bgmBeforeOpenAudio = EazySoundManager.GetAudio(_bgmBeforeOpenID);
            _bgmBeforeOpenAudio.SpatialBlend = 0f;
        }

        if (_bgmAfterOpenAudio == null)
        {
            _bgmAfterOpenID = EazySoundManager.PlaySound(bgmClipAfterOpen, volume, false, gameObject.transform);
            _bgmAfterOpenAudio = EazySoundManager.GetAudio(_bgmAfterOpenID);
            _bgmAfterOpenAudio.SpatialBlend = 0f;
        }
        
        if (_swooshAudio == null)
        {
            _swooshID = EazySoundManager.PlaySound(swooshClip, volume, true, gameObject.transform);
            _swooshAudio = EazySoundManager.GetAudio(_swooshID);
            _swooshAudio.SpatialBlend = 0;
        }
        
        _bgmBeforeOpenAudio.FadeOutSeconds = 0.1f;
        _bgmAfterOpenAudio.FadeOutSeconds = 0.1f;
        _swooshAudio.FadeOutSeconds = 0.1f;
        
        _bgmBeforeOpenAudio.FadeInSeconds = 0.1f;
        _bgmAfterOpenAudio.FadeInSeconds = 0.1f;
        _swooshAudio.FadeInSeconds = 0.1f;
        
        _openDoorAudio.Stop();
        _buzzerAudio.Stop();
        _bgmAfterOpenAudio.Stop();
        _swooshAudio.Stop();
    }

    public void Play(string clipName)
    {
        switch (clipName)
        {
            case "buzzer":
                _buzzerAudio.Play();
                break;
            
            case "door":
                _openDoorAudio.Play();
                break;
            
            case "bgm before open":
                _bgmBeforeOpenAudio.Play();
                break;
            
            case "bgm after open":
                _bgmAfterOpenAudio.Play();
                break;
            
            case "swoosh":
                _swooshAudio.Play();
                break;
        }
    }
    
    public void Stop(string clipName)
    {
        switch (clipName)
        {
            case "buzzer":
                _buzzerAudio.Stop();
                break;
            
            case "door":
                _openDoorAudio.Stop();
                break;
            
            case "bgm before open":
                _bgmBeforeOpenAudio.Stop();
                break;
            
            case "bgm after open":
                _bgmAfterOpenAudio.Stop();
                break;
            
            case "swoosh":
                _swooshAudio.Stop();
                break;
        }
    }

    private void OnDisable()
    {
        _bgmAfterOpenAudio.Stop();
        _buzzerAudio.Stop();
        _bgmBeforeOpenAudio.Stop();
        _openDoorAudio.Stop();
    }
}
