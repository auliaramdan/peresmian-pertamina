using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DoorBuzzer : MonoBehaviour
{
    [SerializeField] private UnityEvent onBuzzerPressed;
    [SerializeField] private UnityEvent onBuzzerUnpressed;
    [SerializeField] private string handTag;

    private bool _pressed = false;
    
    private void OnTriggerEnter(Collider other)
    {
        if (_pressed || !other.CompareTag(handTag))
            return;
        
        onBuzzerPressed?.Invoke();
        _pressed = true;
    }

    private void OnTriggerExit(Collider other) {
        if (!_pressed || !other.CompareTag(handTag))
            return;
        
        onBuzzerUnpressed?.Invoke();
        _pressed = false;
    }
}
