using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using SandboxVRSeason2.Framework;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class ThermoGun : MonoBehaviour
{
    [SerializeField] private float randomInterval = 2;
    [SerializeField] private float minTemp = 35, maxTemp = 45;
    [SerializeField] private SB_TimedCheck timedCheck;
    [SerializeField] private string targetName;
    [SerializeField] private UnityEvent onTargetEnter, onTargetExit;
    [SerializeField] private TextMeshProUGUI thermoText;

    private float _lastTextUpdate = 0f;
    private float _lastThermoUpdate = 0f;
    
    public bool IsChecking { get; private set; }
    public float DistanceToTarget { get; private set; }

    private void Start()
    {
        _lastTextUpdate = Time.time;
        _lastThermoUpdate = Random.Range(minTemp, maxTemp);
        thermoText.text = $"{_lastThermoUpdate:F1}<sup>o</sup>C";
    }

    private void Update()
    {
        if ((_lastTextUpdate - Time.time) > randomInterval)
        {
            _lastTextUpdate = Time.time;
            _lastThermoUpdate = Random.Range(minTemp, maxTemp);
            thermoText.text = $"{_lastThermoUpdate:F1}<sup>o</sup>C";
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == targetName)
        {
            onTargetEnter?.Invoke();
            IsChecking = true;
            DistanceToTarget = Vector3.Distance(transform.position, other.transform.position);
            SetTimedCheckActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == targetName)
        {
            onTargetExit?.Invoke();
            IsChecking = false;
            SetTimedCheckActive(false);
            DistanceToTarget = float.MaxValue;
        }
    }

    public void SetTimedCheckActive(bool value)
    {
        if (timedCheck.isActiveAndEnabled == !value)
        {
            timedCheck.gameObject.SetActive(value);
            thermoText.gameObject.SetActive(value);
        }
    }
}
