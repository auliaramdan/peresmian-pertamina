using System.Collections;
using System.Collections.Generic;
using SandboxVRSeason2.Framework;
using UnityEngine;

public class InitValveRotation : MonoBehaviour
{
    [SerializeField] private GameObject handle;
    
    private Sandbox_ValveController _valveController;

    // Start is called before the first frame update
    void Start()
    {
        _valveController = GetComponent<Sandbox_ValveController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetupValvePercent(float percent)
    {
        StartCoroutine(InitAngle(Mathf.Lerp(_valveController.valveMinAngle, _valveController.valveMaxAngle,
            percent/100) * -1));
        Debug.Log(Mathf.Lerp(_valveController.valveMinAngle, _valveController.valveMaxAngle,
            percent/100) * -1);
    }

    public void SetupValveRaw(float value)
    {
        StartCoroutine(InitAngle(-value));
    }

    private IEnumerator InitAngle(float value)
    {
        yield return new WaitForSeconds(1f);
        handle.transform.localRotation = Quaternion.Euler(0, value, 0);
    }
}