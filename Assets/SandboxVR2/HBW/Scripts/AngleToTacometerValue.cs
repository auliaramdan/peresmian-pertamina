using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AngleToTacometerValue : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI tacometerText;
    [SerializeField] private float minTacometerValue, maxTacometerValue;
    [SerializeField] private float minAngleValue, maxAngleValue;

    public bool CanChange { get; set; } = false;

    private float _tacometerValue = 0;

    public void OnAngleChanged(float value)
    {
        if (!CanChange)
            return;
        
        _tacometerValue = Mathf.Lerp(minTacometerValue, maxTacometerValue,
            Mathf.InverseLerp(minAngleValue, maxAngleValue, value));

        tacometerText.text = _tacometerValue.ToString("F1");
    }
}
