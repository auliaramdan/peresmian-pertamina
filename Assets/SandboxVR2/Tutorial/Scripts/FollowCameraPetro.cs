﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using HurricaneVR.Framework.Core.Player;
using UnityEngine;

public class FollowCameraPetro : MonoBehaviour
{
    public Quaternion rotationMultiplier = new Quaternion(1,1,1,1);
    public float distMoveParameter = 1f;
    private Quaternion targetRotation;
    public float distanceFromCamera = 0.8f;
    private Vector3 targetPosition;
    private bool foundTarget;

    [SerializeField] private float yDistance;
    [SerializeField] private float angleFactor;
    [SerializeField] private float moveSpeed;

    [SerializeField] private float targetAngleFactor = 20;
    [SerializeField] private float targetDistance = 1;

    private float _distanceFromTargetPosition;
    private float _angleBetweenTargetAndSelf;
    private Camera _mainCam;
    private HVRCanvasFade _screenFade;
    private bool _moveCheck = false;

    private void Awake()
    {
        foundTarget = false;
        StartCoroutine(nameof(findTarget));
    }

    private void Start()
    {
        
        _mainCam = Camera.main;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (foundTarget)
        {
            _distanceFromTargetPosition = Vector3.Distance(transform.position, targetPosition);
            _angleBetweenTargetAndSelf = Quaternion.Angle(targetRotation, transform.rotation);
            
            if (!_moveCheck && (_angleBetweenTargetAndSelf > angleFactor ||
                 _distanceFromTargetPosition > distMoveParameter))
            {
                _moveCheck = true;
            }
            
            else if (_moveCheck)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, moveSpeed * Time.deltaTime);
                //Debug.Log("rotating");
                transform.position = Vector3.Lerp(transform.position,targetPosition, moveSpeed  * Time.deltaTime);
                //Debug.Log("moving");

                if (_angleBetweenTargetAndSelf < targetAngleFactor ||
                    _distanceFromTargetPosition < targetDistance)
                {
                    _moveCheck = false;
                }
            }
        }
    }

    IEnumerator findTarget()
    {
        _screenFade = FindObjectOfType<HVRCanvasFade>();
        GameObject q = new GameObject("canvasIndicator");
        while (_screenFade)
        {
            yield return new WaitForSeconds(0.01f);
            Vector3 pos = _screenFade.transform.position + _screenFade.transform.forward * distanceFromCamera;
            q.transform.position = new Vector3(pos.x, _mainCam.transform.position.y + yDistance, pos.z);
            foundTarget = true;
            //position
            targetPosition = q.transform.position;
            //rotation
            q.transform.LookAt(_mainCam.transform);
            targetRotation = q.transform.rotation * rotationMultiplier;
        }
    }
}
