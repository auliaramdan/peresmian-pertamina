using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using HurricaneVR.Framework.Shared;
using Tictech.LoadManager;
using UnityEngine;

public class TutorialMenu : MonoBehaviour
{
    [Header("Main")]
    [ SerializeField] private Rigidbody canvasMain;
    [SerializeField] private Transform canvasCallTargetPos;
    [SerializeField] private GameObject igmPanel;
    
    private float callPressButtonTime;
    private bool callPressing;
    private Coroutine callCoroutine;
    
    // Start is called before the first frame update
    void Start()
    {
        canvasCallTargetPos = GameObject.FindGameObjectsWithTag("UITargetPos")[0].transform;
    }

    // Update is called once per frame
    void Update()
    {
        CallCanvas();
    }
    
    void CallCanvas()
    {
        if (callPressing)
            callPressButtonTime += Time.deltaTime;

        var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);

        if (buttonState.JustActivated)
        {
            callPressing = true;
        }

        if (buttonState.JustDeactivated)
        {
            callPressing = false;
            if (callPressButtonTime <= 1f)
            {
                if (callCoroutine != null) StopCoroutine(callCoroutine);
                //callCoroutine = StartCoroutine(CallCanvasCoroutine());
            }

            callPressButtonTime = 0;
        }

        if (callPressButtonTime > 1f)
        {
            igmPanel.gameObject.SetActive(true);
        }
    }

    public void BackToLobby()
    {
        RuntimeManager.LoadScene(RuntimeManager.Instance.defaultScene);
    }

    public void QuitGame(){
        Application.Quit();
    }
}
