using System.Collections;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;

public class UIPopUp : MonoBehaviour
{
    [SerializeField] private bool openOnInstantiated = true;
    [SerializeField] private bool useVectorOne = true;
    
    private bool _readyToTransform = true;
    private Vector3 _openedScale = Vector3.one, _closedScale = Vector3.zero;

    public bool IsOpen { get; private set; } = true;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!useVectorOne)
        {
            _openedScale = transform.localScale;
        }
        
        if (!openOnInstantiated)
        {
            ShowUI(false);
        }
    }
    
    public void ShowUI(bool show)
    {
        StartCoroutine(_ShowUI(show));
    }

    IEnumerator _ShowUI(bool value)
    {
        if (IsOpen != value)
        {
            IsOpen = value;
            //_readyToTransform = false;
            yield return transform.DOScale(value ? _openedScale : _closedScale, 0.5f).WaitForCompletion();
            //_readyToTransform = true;
        }
    }
}
