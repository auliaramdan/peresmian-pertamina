﻿using System;
using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Shared;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class ScenarioTutorialButtonTimedInteraction : MonoBehaviour
{
    [SerializeField] private HVRXRInputFeatures requiredState;
    [SerializeField] private bool requiredRightController;
    
    [Tooltip("Using seconds")]
    [SerializeField] private float requiredTime = 0;
    
    [SerializeField] private UnityEvent onRequiredReach;

    [SerializeField] private TextMeshProUGUI counterHolder;

    private float _timeElapsed = 0;
    private bool _completed = false;
    private HVRController _requiredController;

    private void Start()
    {
        _requiredController = requiredRightController
            ? HVRInputManager.Instance.RightController
            : HVRInputManager.Instance.LeftController;
    }

    // Update is called once per frame
    void Update()
    {
        switch (requiredState)
        {
            case HVRXRInputFeatures.PrimaryButton :
                CheckRequiredButtonPressed(_requiredController.PrimaryButtonState.Active);
                break;
            case HVRXRInputFeatures.SecondaryButton :
                CheckRequiredButtonPressed(_requiredController.SecondaryButtonState.Active);
                break;
            case HVRXRInputFeatures.Trigger :
                CheckRequiredButtonPressed(_requiredController.TriggerButtonState.Active);
                break;
            case HVRXRInputFeatures.Grip :
                CheckRequiredButtonPressed(_requiredController.GripButtonState.Active);
                break;
        }

        if (_timeElapsed >= requiredTime - 0.1f && !_completed)
        {
            onRequiredReach?.Invoke();
            if(counterHolder)
                counterHolder.text = "(100/100)";
            _completed = true;
        }
    }
    
    private void CheckRequiredButtonPressed(bool pressed)
    {
        if (pressed && _timeElapsed < requiredTime)
        {
            _timeElapsed += Time.deltaTime;
            
            if (counterHolder)
            {
                counterHolder.text = $"({(Mathf.InverseLerp(0, requiredTime, _timeElapsed) * 100):F2}/100)";
            }
        }
    }
}