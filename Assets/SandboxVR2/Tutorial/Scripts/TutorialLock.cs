using System.Collections;
using System.Collections.Generic;
using HurricaneVR.Framework.Core.Player;
using UnityEngine;

public class TutorialLock : MonoBehaviour
{
    [SerializeField] private HVRPlayerController playerController;
    [SerializeField] private HVRTeleporter playerTeleportHandler;
    [SerializeField] private bool enableMovement, enableRotation, enableTeleportation, enableCrouching;
    
    // Start is called before the first frame update
    void Start()
    {
        playerController = GetComponent<HVRPlayerController>();
        playerTeleportHandler = GetComponent<HVRTeleporter>();
        
        playerController.MovementEnabled = enableMovement;
        playerController.RotationEnabled = enableRotation;
        playerController.CanCrouch = enableCrouching;
        playerTeleportHandler.enabled = enableTeleportation;
    }

    public void SetRotationEnabled(bool value)
    {
        playerController.RotationEnabled = value;
        enableRotation = value;
    }

    public void SetMovementEnabled(bool value)
    {
        playerController.MovementEnabled = value;
        enableMovement = value;
    }

    public void SetTeleportationEnabled(bool value)
    {
        playerTeleportHandler.enabled = value;
        enableTeleportation = value;
    }

    public void SetCrouchingEnabled(bool value)
    {
        playerController.CanCrouch = value;
        enableCrouching = value;
    }
}
