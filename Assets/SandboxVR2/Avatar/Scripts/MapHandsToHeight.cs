using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapHandsToHeight : MonoBehaviour
{
    [SerializeField] private Transform head;
    [SerializeField] private Transform leftHand;
    [SerializeField] private Transform rightHand;
    [SerializeField] private Transform mappedLeftHand;
    [SerializeField] private Transform mappedRightHand;
    //[SerializeField] private Transform

    [SerializeField] private LayerMask groundMask;
    
    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(head.position, head.up * -1);
        Debug.DrawRay(head.position, head.up * -1, Color.green);

        RaycastHit hit;
        float floorHeightOffset = 0;
        
        if(Physics.Raycast(ray, out hit, 10, groundMask))
        {
            floorHeightOffset = hit.point.y;
        }
        
        float headHeight = head.TransformPoint(transform.position).y - floorHeightOffset;
        float leftHandHeight = head.TransformPoint(transform.position).y - floorHeightOffset;
        
        //mappedLeftHand.transform.position = new Vector3(mappedLeftHand.position.x, Mathf.Lerp())
    }
}
