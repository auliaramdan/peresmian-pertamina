using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKFootSolve : MonoBehaviour
{
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private float maxStepLength;
    [SerializeField] private Transform centerTransform;
    [SerializeField] private float stepHeight = 1;
    [SerializeField] private float speed = 1;
    
    private Vector3 _previousPosition, _currentPosition, _nextPosition;
    private Vector3 _previousNormal, _currentNormal, _nextNormal;
    private float _stepLerp = 1;
    private float _footSpacing;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _footSpacing = transform.localPosition.x;
        _previousPosition = transform.position;
        _currentPosition = _nextPosition = _previousPosition;

        _previousNormal = transform.up;
        _currentNormal = _nextNormal = _previousNormal;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = _currentPosition;
        transform.up = _currentNormal;
        
        
        
        if (_stepLerp < 1)
        {
            Vector3 temp = Vector3.Lerp(_previousPosition, _nextPosition, _stepLerp);
            temp.y += Mathf.Sin(_stepLerp * Mathf.PI) * stepHeight;

            _currentPosition = temp;
            _currentNormal = Vector3.Lerp(_previousNormal, _nextNormal, _stepLerp);
            _stepLerp += Time.deltaTime * speed;
        }

        else
        {
            _previousNormal = _nextNormal;
            _previousPosition = _nextPosition;
            
            CheckRaycast();
        }
    }

    private void CheckRaycast()
    {
        Ray ray = new Ray(centerTransform.position + (Vector3.right * _footSpacing), Vector3.down);
        RaycastHit hit;
        
        Debug.DrawRay(centerTransform.position + (centerTransform. right * _footSpacing), Vector3.down, Color.red);
        
        if (Physics.Raycast(ray, out hit, 10, groundLayer))
        {
            if (Vector3.Distance(_nextPosition, hit.point) > maxStepLength)
            {
                _stepLerp = 0;
                int direction = centerTransform.InverseTransformDirection(hit.point).x > centerTransform.InverseTransformDirection(_nextPosition).x ? 1 : -1;
                _nextPosition = hit.point + (centerTransform.forward * (maxStepLength * direction));
                _nextNormal = hit.normal;
            }
        }
    }
}
