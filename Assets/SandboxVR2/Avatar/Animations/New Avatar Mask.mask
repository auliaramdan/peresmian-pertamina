%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: New Avatar Mask
  m_Mask: 01000000000000000000000001000000010000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Body
    m_Weight: 1
  - m_Path: Bone001
    m_Weight: 1
  - m_Path: Bone001/Bone002
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone003
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone003/Bone004
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone003/Bone004/Bone005
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone003/Bone004/Bone005/Bone006
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone079
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone079/Bone080
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone079/Bone080/Bone081
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone079/Bone080/Bone081/Bone082
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone079/Bone080/Bone081/Bone082/Bone083
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone084
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone084/Bone085
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone084/Bone085/Bone086
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone084/Bone085/Bone086/Bone087
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone084/Bone085/Bone086/Bone087/Bone088
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone089
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone089/Bone090
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone089/Bone090/Bone091
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone089/Bone090/Bone091/Bone092
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone089/Bone090/Bone091/Bone092/Bone093
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone094
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone094/Bone095
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone094/Bone095/Bone096
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone094/Bone095/Bone096/Bone097
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone094/Bone095/Bone096/Bone097/Bone098
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone099
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone099/Bone100
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone099/Bone100/Bone101
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone075/Bone076/Bone077/Bone078/Bone099/Bone100/Bone101/Bone102
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone107
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone107/Bone108
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone107/Bone108/Bone109
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone107/Bone108/Bone109/Bone110
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone107/Bone108/Bone109/Bone110/Bone111
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone112
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone112/Bone113
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone112/Bone113/Bone114
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone112/Bone113/Bone114/Bone115
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone112/Bone113/Bone114/Bone115/Bone116
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone117
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone117/Bone118
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone117/Bone118/Bone119
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone117/Bone118/Bone119/Bone120
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone117/Bone118/Bone119/Bone120/Bone121
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone122
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone122/Bone123
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone122/Bone123/Bone124
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone122/Bone123/Bone124/Bone125
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone122/Bone123/Bone124/Bone125/Bone126
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone127
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone127/Bone128
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone127/Bone128/Bone129
    m_Weight: 1
  - m_Path: Bone001/Bone002/Bone103/Bone104/Bone105/Bone106/Bone127/Bone128/Bone129/Bone130
    m_Weight: 1
  - m_Path: Bone001/Bone007
    m_Weight: 1
  - m_Path: Bone001/Bone007/Bone008
    m_Weight: 1
  - m_Path: Bone001/Bone007/Bone008/Bone009
    m_Weight: 1
  - m_Path: Bone001/Bone007/Bone008/Bone009/Bone010
    m_Weight: 1
  - m_Path: Bone001/Bone007/Bone008/Bone009/Bone010/Bone011
    m_Weight: 1
  - m_Path: Bone001/Bone007/Bone008/Bone009/Bone010/Bone011/Bone012
    m_Weight: 1
  - m_Path: Bone001/Bone069
    m_Weight: 1
  - m_Path: Bone001/Bone069/Bone070
    m_Weight: 1
  - m_Path: Bone001/Bone069/Bone070/Bone071
    m_Weight: 1
  - m_Path: Bone001/Bone069/Bone070/Bone071/Bone072
    m_Weight: 1
  - m_Path: Bone001/Bone069/Bone070/Bone071/Bone072/Bone073
    m_Weight: 1
  - m_Path: Bone001/Bone069/Bone070/Bone071/Bone072/Bone073/Bone074
    m_Weight: 1
  - m_Path: Bottoms
    m_Weight: 1
  - m_Path: Eyelashes
    m_Weight: 1
  - m_Path: Eyes
    m_Weight: 1
  - m_Path: Eyewear
    m_Weight: 1
  - m_Path: Gloves
    m_Weight: 1
  - m_Path: Hats
    m_Weight: 1
  - m_Path: IK Chain001
    m_Weight: 1
  - m_Path: IK Chain002
    m_Weight: 1
  - m_Path: Masks
    m_Weight: 1
  - m_Path: Shoes
    m_Weight: 1
  - m_Path: Tops
    m_Weight: 1
