using System.IO;
using UnityEngine;
using Sirenix.OdinInspector;

namespace SandboxVRSeason2.Framework
{
    public class Paintable : Sandbox_Object
    {
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _linearLock;
        public override bool linearLock
        {
            get { return _linearLock; }
            set
            {
                if (!_linearException)// && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _linearLock = value;
                    if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                        foreach (var outline in outlineList) 
                            outline.enabled = !value;
                }
            }
        }

        const int TEXTURE_SIZE = 1024;

        public float extendsIslandOffset = 1;

        RenderTexture extendIslandsRenderTexture;
        RenderTexture uvIslandsRenderTexture;
        RenderTexture maskRenderTexture;
        RenderTexture supportTexture;

        Renderer rend;

        int maskTextureID = Shader.PropertyToID("_MaskTexture");

        public RenderTexture getMask() => maskRenderTexture;
        public RenderTexture getUVIslands() => uvIslandsRenderTexture;
        public RenderTexture getExtend() => extendIslandsRenderTexture;
        public RenderTexture getSupport() => supportTexture;
        public Renderer getRenderer() => rend;

        [Button]
        void SaveRenderTextureMask()
        {
            SaveTexturePNG(toTexture2D(maskRenderTexture));
        }


        void Start()
        {
            maskRenderTexture = new RenderTexture(TEXTURE_SIZE, TEXTURE_SIZE, 0);
            maskRenderTexture.filterMode = FilterMode.Bilinear;

            extendIslandsRenderTexture = new RenderTexture(TEXTURE_SIZE, TEXTURE_SIZE, 0);
            extendIslandsRenderTexture.filterMode = FilterMode.Bilinear;

            uvIslandsRenderTexture = new RenderTexture(TEXTURE_SIZE, TEXTURE_SIZE, 0);
            uvIslandsRenderTexture.filterMode = FilterMode.Bilinear;

            supportTexture = new RenderTexture(TEXTURE_SIZE, TEXTURE_SIZE, 0);
            supportTexture.filterMode = FilterMode.Bilinear;

            rend = GetComponent<Renderer>();

            if (rend.material.GetTexture(maskTextureID) != null)
            {
                Graphics.Blit(rend.material.GetTexture(maskTextureID), maskRenderTexture);
                Graphics.Blit(rend.material.GetTexture(maskTextureID), uvIslandsRenderTexture);
                Graphics.Blit(rend.material.GetTexture(maskTextureID), extendIslandsRenderTexture);
                Graphics.Blit(rend.material.GetTexture(maskTextureID), supportTexture);
            }

            rend.material.SetTexture(maskTextureID, extendIslandsRenderTexture);

            PaintManager.instance.initTextures(this);
        }

        void OnDisable()
        {
            maskRenderTexture.Release();
            uvIslandsRenderTexture.Release();
            extendIslandsRenderTexture.Release();
            supportTexture.Release();
        }

        // Save Texture as PNG
        void SaveTexturePNG(Texture2D tex)
        {

            // Encode texture into PNG
            byte[] bytes = tex.EncodeToPNG();
            Object.Destroy(tex);

            // For testing purposes, also write to a file in the project folder
            File.WriteAllBytes(Application.dataPath + "/../SavedScreen.png", bytes);
            Debug.Log("File saved to /../SavedScreen.png");
        }
        //convert render texture to texture2d
        public Texture2D toTexture2D(RenderTexture rTex)
        {
            Texture2D tex = new Texture2D(1024, 1024, TextureFormat.RGB24, false);
            // ReadPixels looks at the active RenderTexture.
            RenderTexture.active = rTex;
            tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
            tex.Apply();
            return tex;
        }
        //compare two texture2d to check if they're identical
        public bool CompareTexture(Texture2D first, Texture2D second, int pixelTolerance)
        {
            Color[] firstPix = first.GetPixels();
            Color[] secondPix = second.GetPixels();
            int wrongPixel = 0;
            if (firstPix.Length != secondPix.Length)
            {
                Debug.Log("beda ukuran");
                return false;
            }
            for (int i = 0; i < firstPix.Length; i++)
            {
                if (firstPix[i] != secondPix[i])
                {
                    wrongPixel++;
                    if (wrongPixel > pixelTolerance)
                    {
                        Debug.Log("beda warna melebihi toleransi");
                        return false;
                    }
                    else
                    {
                        Debug.Log("beda warna masih diolerir");
                    }
                }
            }
            Debug.Log("identik");
            return true;
        }
    }
}