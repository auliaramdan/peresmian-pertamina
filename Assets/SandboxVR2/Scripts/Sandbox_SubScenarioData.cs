using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;
using Tictech.LoadManager;

namespace SandboxVRSeason2.Framework
{
    [CreateAssetMenu(fileName = "New Sub Scenario Data", menuName = "Sandbox/Sub Scenario Data", order = 0)]
    public class Sandbox_SubScenarioData : ScriptableObject
    {
        public string SubScenarioName;
        public Sprite SubScenarioSprite;
        public AssetReference SubScenarioScene;
        public AssetReference SubScenarioPrefab;

        public bool isLinear = true;
        public bool isSandbox = true;

    }
}
