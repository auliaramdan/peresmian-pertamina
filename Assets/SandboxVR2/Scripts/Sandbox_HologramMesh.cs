using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_HologramMesh : MonoBehaviour
    {
        private SB_Item item = null;

        public void SetAction(SB_Item _item)
        {
            item = _item;
            item.hologramAction += SetHologramMesh;
        }

        private void SetHologramMesh(bool value)
        {
            GetComponent<MeshRenderer>().enabled = value;
        }

        private void OnDestroy()
        {
            item.hologramAction -= SetHologramMesh;
        }
    }
}