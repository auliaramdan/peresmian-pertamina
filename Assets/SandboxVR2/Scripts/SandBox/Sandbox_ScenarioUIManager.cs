using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using System;
using UnityEngine.AddressableAssets;
using Tictech.LoadManager;
using UnityEngine.SceneManagement;
using HurricaneVR.Framework.Shared;
using DG.Tweening;
using System.Linq;
using UnityEngine.Events;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_ScenarioUIManager : MonoBehaviour
    {
        public static Sandbox_ScenarioUIManager instance;

        [Header("Main")]
        [FoldoutGroup("Main"), SerializeField] private Rigidbody canvasMain;
        [FoldoutGroup("Main"), SerializeField] private Transform canvasCallTargetPos;
        private float callPressButtonTime;
        private bool callPressing;
        private Coroutine callCoroutine;

        [Header("Instruction")]
        [FoldoutGroup("Instruction"), SerializeField] private Sandbox_InstructionData instructionData;
        [FoldoutGroup("Instruction"), SerializeField] private Transform instructionCanvas;
        [FoldoutGroup("Instruction"), SerializeField] private Transform instructionPanel;
        [FoldoutGroup("Instruction"), SerializeField] private TextMeshProUGUI insTitle;
        [FoldoutGroup("Instruction"), SerializeField] private Image insImage;
        [FoldoutGroup("Instruction"), SerializeField] private TextMeshProUGUI insContent;
        int instructionIndex;

        [Header("Section List")]
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sectionListObject;
        [FoldoutGroup("SectionList"), SerializeField] private Transform sectionListContentParent;
        [FoldoutGroup("SectionList"), SerializeField] private AssetReference sectionListPrefab; //SB_UIReference

        [Header("Section Detail")] //sd = Section Detail
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sdObject;
        [FoldoutGroup("SectionList"), SerializeField] private Transform sdContentParent;
        [FoldoutGroup("SectionList"), SerializeField] private AssetReference sdDescPrefab;
        [FoldoutGroup("SectionList"), SerializeField] private AssetReference sdItemPrefab;
        [FoldoutGroup("SectionList"), SerializeField] private AssetReference sdItemZeroPrefab;
        [FoldoutGroup("SectionList"), SerializeField] private GameObject sdBackButton;
        [FoldoutGroup("SectionList"), SerializeField] private TextMeshProUGUI sdTitle;
        [FoldoutGroup("SectionList"), SerializeField] public Animator sdCompleteAnimator;
        [FoldoutGroup("SectionList"), SerializeField, ReadOnly] public Sandbox_Section selectedSection;

        [FoldoutGroup("SectionList"), SerializeField]
        private Sandbox_SubScenarioData next;

        [FoldoutGroup("TimedCheck")] public GameObject checkCanvas;
        [FoldoutGroup("TimedCheck")] public TextMeshProUGUI checkTitleText;
        [FoldoutGroup("TimedCheck")] public Image checkFillImage;
        [FoldoutGroup("TimedCheck")] public GameObject checkCompleteImage;
        [FoldoutGroup("TimedCheck")] public GameObject checkCompleteImageSpawn;


        [FoldoutGroup("Complete"), SerializeField] public SB_UIReference completePanel;

        [FoldoutGroup("IGM"), SerializeField] public SB_UIReference igmPanel;

        [FoldoutGroup("Others"), SerializeField] public Sprite completeIcon;
        [FoldoutGroup("Others"), SerializeField] public Sprite incompleteIcon;
        [FoldoutGroup("Others"), SerializeField] public Sprite infoOnlyIcon;
        [FoldoutGroup("Others"), SerializeField] private GameObject LoadingCanvas;
        [FoldoutGroup("Others"), SerializeField, ReadOnly] public bool isInit;
        

        Coroutine setUpSectionListCor;
        Coroutine setUpSectionDetailCor;


        bool referenceLoaded;
        bool sectionListInit;
        [HideInInspector] public bool sectionDetailsInit;
        private void Awake()
        {
            if (instance != null && instance != this)
                Destroy(this.gameObject);
            else
                instance = this;
        }

        private void Start()
        {
            StartCoroutine(Initialize());
            canvasCallTargetPos = GameObject.FindGameObjectsWithTag("UITargetPos")[0].transform;
        }

        private void Update()
        {
            CallCanvas();

            if (callPressing)
                callPressButtonTime += Time.deltaTime;

            var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);

            if (buttonState.JustActivated)
                callPressing = true;

            if (buttonState.JustDeactivated)
            {
                callPressing = false;
                if (callPressButtonTime <= 1f)
                {
                    if (callCoroutine != null) StopCoroutine(callCoroutine);
                    callCoroutine = StartCoroutine(CallCanvasCoroutine());
                }

                callPressButtonTime = 0;
            }

            if (callPressButtonTime > 1f)
            {
                igmPanel.gameObject.SetActive(true);
            }
        }

        private IEnumerator Initialize()
        {
            print("SB : Initiating Scenario UI Manager");
            yield return new WaitUntil(() => Sandbox_ScenarioManager.instance.isInit == true);

            yield return SetUpSectionDetailCoroutine();

            if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.SANDBOX)
            {
                yield return SetUpSectionListCoroutine();
            }
            else
            {
                OpenStepDetail(Sandbox_ScenarioManager.instance.sectionList[0]);
                sdBackButton.SetActive(false);
            }

            igmPanel.customButtonList[0].onDown.AddListener(() => { RuntimeManager.LoadScene(RuntimeManager.Instance.defaultScene); });
            //completePanel.customButtonList[0].onDown.AddListener(() => { RuntimeManager.LoadScene(RuntimeManager.Instance.defaultScene); });
            completePanel.customButtonList[1].onDown.AddListener(() =>
            {
                print("Kepencet");
                RuntimeManager.LoadScenario(RuntimeManager.Instance.currentSubScenario, Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR ? true : false); 
            });

            print("SB : Scenario UI Manager Initiated");

            isInit = true;

            SetupUInstructionList();
        }

        public void SetupUInstructionList()
        {
            if (!instructionPanel || !instructionData) return;
            instructionCanvas.gameObject.SetActive(true);
            instructionIndex = 0;
            StartCoroutine(fillInstruction(instructionIndex));
            SB_UIButton sbButton = instructionPanel.GetComponentInChildren<SB_UIButton>();

            //sectionListObject.gameObject
        }


        ScaleObjectToCanvas scaler;
        public IEnumerator fillInstruction(int index)
        {
            //Debug.Log("berubah jadi " + index);
            if (instructionData.instructions.Count < index + 1)
            {
                if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.SANDBOX)
                {
                    sectionListObject.gameObject.SetActive(true);
                    sdObject.gameObject.SetActive(false);
                }
                else
                {
                    sectionListObject.gameObject.SetActive(false);
                    sdObject.gameObject.SetActive(true);
                }
                instructionCanvas.gameObject.SetActive(false);

                Sandbox_ScenarioManager.instance.StartScenario();
                instructionIndex = 0;
            }
            else
            {
                insTitle.text = instructionData.instructions[index].title;
                insImage.sprite = instructionData.instructions[index].image;
                insContent.text = instructionData.instructions[index].content;
                instructionIndex = index;

                SB_UIButton sbButton = instructionPanel.GetComponentInChildren<SB_UIButton>();
                sbButton.onDown = new UnityEvent();
                sbButton.onDown.AddListener(() => StartCoroutine(fillInstruction(instructionIndex + 1)));

                LayoutRebuilder.ForceRebuildLayoutImmediate(instructionPanel.GetComponent<RectTransform>());
                yield return new WaitForEndOfFrame();
                sdObject.gameObject.SetActive(false);
                sectionListObject.gameObject.SetActive(false);
                if (!scaler)
                    if (instructionCanvas.GetComponentInChildren<ScaleObjectToCanvas>())
                        scaler = instructionCanvas.GetComponentInChildren<ScaleObjectToCanvas>();
                    else scaler = null;
                if (scaler)
                    scaler.scaleToCanvas();

                yield return new WaitForEndOfFrame();
                if (instructionData.instructions.Count <= index + 1)
                    sbButton.GetComponentInChildren<TextMeshProUGUI>().text = "Tutup";
                instructionPanel.transform.localScale = Vector3.zero;
                instructionPanel.transform.DOScale(Vector3.one, .25f);

                yield return new WaitForSeconds(.30f);
            }
        }

        public void SetUpSectionList()
        {
            if (setUpSectionListCor != null) StopCoroutine(setUpSectionListCor);
            setUpSectionListCor = StartCoroutine(SetUpSectionListCoroutine());
        }

        public IEnumerator SetUpSectionListCoroutine()
        {
            if (sectionListInit) yield break;
            sectionListInit = true;
            LoadingCanvas.SetActive(true);

            foreach (Transform child in sectionListContentParent)
                Destroy(child.gameObject);

            foreach (var section in Sandbox_ScenarioManager.instance.sectionList)
            {
                SB_UIReference _section = null;
                sectionListPrefab.Spawn<SB_UIReference>(sectionListContentParent, (result) => { _section = result; });

                yield return new WaitUntil(() => _section != null);
                section.uiReference = _section;
                UpdateSectionList(_section, section);
            }

            if (instructionCanvas.gameObject.activeSelf)
            {
                instructionCanvas.gameObject.SetActive(true);
                sectionListObject.SetActive(false);
            }
            else
            {
                instructionCanvas.gameObject.SetActive(false);
                sectionListObject.SetActive(true);
            }
            sdObject.SetActive(false);
            LoadingCanvas.SetActive(false);
        }
        public void UpdateSectionList(SB_UIReference _target, Sandbox_Section _section)
        {
            if (_target == null) return;
            print("Meraung " + _target);
            var _sectionNameText = _target.tmProList[0];
            _sectionNameText.text = _section.sectionName;

            var _sectionOpenButton = _target.customButtonList[0];
            _sectionOpenButton.onDown.RemoveAllListeners();
            _sectionOpenButton.onDown.AddListener(() =>
            {
                OpenStepDetail(_section);
            });


            var _notEmptyStep = _section.stepList.Find(p => p.stepCountTarget != 0);

            var _uncompleteStep = _section.stepList.Find(p => p.stepCountCurrent < p.stepCountTarget);

            var _sectionCompleteIcon = _target.imageList[0];

            if (_notEmptyStep == null) _sectionCompleteIcon.sprite = infoOnlyIcon;
            else if (_uncompleteStep == null) _sectionCompleteIcon.sprite = completeIcon;
        }

        public void SetUpSectionDetail(Sandbox_Section _section)
        {
            if (setUpSectionDetailCor != null) StopCoroutine(setUpSectionDetailCor);
            setUpSectionDetailCor = StartCoroutine(SetUpSectionDetailCoroutine());
        }

        public IEnumerator SetUpSectionDetailCoroutine()
        {
            LoadingCanvas.SetActive(true);

            foreach (Transform child in sdContentParent)
                Destroy(child.gameObject);
            foreach (var section in Sandbox_ScenarioManager.instance.sectionList)
            {
                if (section.sectionDesc != "")
                {
                    SB_UIReference _desc = null;
                    sdDescPrefab.Spawn<SB_UIReference>(sdContentParent, (result) => { _desc = result; });
                    yield return new WaitUntil(() => _desc != null);
                    section.descUIReference = _desc;
                    _desc.gameObject.SetActive(false);

                }
                foreach (var step in section.stepList)
                {
                    SB_UIReference _SectionItem = null;
                    if (step.stepCountTarget <= 0)
                        sdItemZeroPrefab.Spawn<SB_UIReference>(sdContentParent, (result) => { _SectionItem = result; });
                    else
                        sdItemPrefab.Spawn<SB_UIReference>(sdContentParent, (result) => { _SectionItem = result; });
                    yield return new WaitUntil(() => _SectionItem != null);
                    section.stepUIReference.Add(_SectionItem);
                    _SectionItem.gameObject.SetActive(false);
                }
                section.UpdateUI();
            }
            sectionDetailsInit = true;
            LoadingCanvas.SetActive(false);
        }
        public void OpenStepDetail(Sandbox_Section _section)
        {
            if (selectedSection)
            {
                if (selectedSection.descUIReference)
                    selectedSection.descUIReference.gameObject.SetActive(false);
                foreach (var refs in selectedSection.stepUIReference)
                {
                    refs.gameObject.SetActive(false);
                }
            }

            selectedSection = _section;
            sdTitle.text = _section.sectionName;

            if (selectedSection.descUIReference)
                selectedSection.descUIReference.gameObject.SetActive(true);
            foreach (var refs in selectedSection.stepUIReference)
            {
                refs.gameObject.SetActive(true);
            }

            if (instructionCanvas.gameObject.activeSelf)
            {
                instructionCanvas.gameObject.SetActive(true);
                sdObject.SetActive(false);
            }
            else
            {
                instructionCanvas.gameObject.SetActive(false);
                sdObject.SetActive(true);
            }
            LoadingCanvas.SetActive(false);
            sectionListObject.SetActive(false);

        }
        void CallCanvas()
        {
            if (callPressing)
                callPressButtonTime += Time.deltaTime;

            var buttonState = HVRController.GetButtonState(HVRHandSide.Left, HVRButtons.Secondary);

            if (buttonState.JustActivated)
            {
                callPressing = true;
            }

            if (buttonState.JustDeactivated)
            {
                callPressing = false;
                if (callPressButtonTime <= 1f)
                {
                    if (callCoroutine != null) StopCoroutine(callCoroutine);
                    callCoroutine = StartCoroutine(CallCanvasCoroutine());
                }

                callPressButtonTime = 0;
            }

            if (callPressButtonTime > 1f)
            {
                igmPanel.gameObject.SetActive(true);
            }
        }

        private IEnumerator CallCanvasCoroutine()
        {
            canvasMain.detectCollisions = false;

            Sequence seq = DOTween.Sequence();
            seq.Append(canvasMain.transform.DOMove(canvasCallTargetPos.position, 0.4f));
            seq.Insert(0, canvasMain.transform.DOLookAt(Camera.main.transform.position, 0.4f, AxisConstraint.Y));
            yield return new WaitForSeconds(0.4f);
            canvasMain.detectCollisions = true;
        }

        public void CompleteScenario()
        {
            var span = TimeSpan.FromSeconds((double)(new decimal(Sandbox_ScenarioManager.instance.playTime)));
            completePanel.tmProList[0].text = span.Minutes + ":" + span.Seconds;
            completePanel.tmProList[1].text = $"{Sandbox_ScenarioManager.instance.playScore:F0}%";
            completePanel.gameObject.SetActive(true);
            Sandbox_ScenarioManager.instance.SetupScore(span.Minutes + ":" + span.Seconds);
        }

        public void LoadScenario(Sandbox_SubScenarioData data)
        {
            RuntimeManager.LoadScenario(data, Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR);
        }
    }
}
