using SandboxVRSeason2.Framework;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisstepManager : MonoBehaviour
{
    [System.Serializable]
    public class sectionStatus
    {
        public bool sectionComplete;
    }

    public Sandbox_ScenarioManager scenarioManager;
    [SerializeField, ReadOnly] List<sectionStatus> checkList;
    [SerializeField, TextArea, ReadOnly] List<string> MisstepList;
    [SerializeField, ReadOnly] List<int> foundIncomplete;
    [SerializeField, ReadOnly] bool Init = false;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);
        if (!scenarioManager)
            scenarioManager = GetComponent<Sandbox_ScenarioManager>() ? GetComponent<Sandbox_ScenarioManager>() : FindObjectOfType<Sandbox_ScenarioManager>();

        if (!Init)
            foreach (var x in scenarioManager.sectionList)
            {
                //Debug.Log("AddListener : " + sumberMasalah.sectionList.IndexOf(x));
                x.onSectionComplete.AddListener(() => CheckSectionsForMisstep(scenarioManager.sectionList.IndexOf(x)));
            }
        Init = true;
    }

    public void CheckSectionsForMisstep(int checkIndex)
    {
        //Debug.Log("Run Check Section : " + checkIndex);
        foundIncomplete = new List<int>();
        foreach (var item in scenarioManager.sectionList)
        {
            int index = scenarioManager.sectionList.IndexOf(item);
            /*if (checkIndex < index)
                break;*/

            if (checkList.Count < index + 1)
                checkList.Add(new sectionStatus { sectionComplete = item.isCompleted });
            
            if(!item.isCompleted && !item.skipMistakeCount)
                foundIncomplete.Add(index + 1);
            
            if (item.isCompleted != checkList[index].sectionComplete)
            {
                checkList[index].sectionComplete = item.isCompleted;
            }
            
            if (foundIncomplete.Count > 0 && !item.skipMistakeCount)
            {
                if (foundIncomplete.Exists(x => x == index + 1))
                    continue;
                string masalah = "section " + (index + 1) + " diselesaikan sebelum menyelesaikan section ";
                foreach (var item2 in foundIncomplete)
                {
                    masalah += (item2 + ", ");
                }
                Debug.Log(masalah);
                if (!MisstepList.Contains(masalah))
                    MisstepList.Add(masalah);
            }
        }
    }

    public void AddMisstepList(string masalah)
    {
        if (!MisstepList.Contains(masalah))
            MisstepList.Add(masalah);
    }
}
