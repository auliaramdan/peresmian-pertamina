using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using System.Linq;
using System.IO;
using HurricaneVR.Framework.Core.Player;
using DG.Tweening;
using Sinbad;
using Tictech.EnterpriseUniversity;
using Tictech.LoadManager;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_ScenarioManager : MonoBehaviour
    {
        public static Sandbox_ScenarioManager instance;
        public ScenarioModeEnum mode;
        public List<Sandbox_Section> sectionList;

        public Material highlightMaterial;

        [FoldoutGroup("Debug"), ReadOnly] public bool isInit;
        [FoldoutGroup("Debug"), ReadOnly] public bool isStarted;
        [FoldoutGroup("Debug"), ReadOnly] private bool isTimePause;
        [FoldoutGroup("Debug"), ReadOnly] public float playTime;
        [FoldoutGroup("Debug"), ReadOnly] public float playScore;
        [FoldoutGroup("Debug"), ReadOnly] public Sandbox_Section currentSection;

        private List<ScoreValue> _scores = new List<ScoreValue>();


        private void Awake()
        {
            if (instance != null && instance != this)
                Destroy(this.gameObject);
            else
                instance = this;
            ScreenFade(1, 0);
        }

        private void Start()
        {
            if (File.Exists(Application.dataPath + "/scores.csv"))
                _scores = CsvUtil.LoadObjects<ScoreValue>(Application.dataPath + "/scores.csv");
        }

        public List<GameObject> playerList;

        private void Update()
        {
            TimeCount();
            if (Input.GetKeyDown(KeyCode.Backspace))
                if (playerList.Count > 1)
                {
                    if (Sandbox_RadioManager.instance)
                        if (Sandbox_RadioManager.instance.radioCanvasLookTarget == Camera.main.transform)
                            Sandbox_RadioManager.instance.radioCanvasLookTarget = null;
                    GameObject activePlayer = playerList.Find(x => x.gameObject.activeSelf);
                    int activeIndex = playerList.IndexOf(activePlayer);
                    playerList[activeIndex].SetActive(false);
                    int newIndex = 0;
                    newIndex = activeIndex + 1 < playerList.Count ? activeIndex + 1 : 0;
                    playerList[newIndex].SetActive(true);
                    playerList[newIndex].transform.position = playerList[activeIndex].transform.position;
                    playerList[newIndex].transform.rotation = playerList[activeIndex].transform.rotation;
                    if (Sandbox_RadioManager.instance)
                    {
                        if (Sandbox_RadioManager.instance.radioCanvasLookTarget == null)
                            Sandbox_RadioManager.instance.radioCanvasLookTarget = Camera.main.transform;
                        Sandbox_RadioManager.instance.moveSocket();
                    }
                }
        }

        public void Initialize()
        {
            StartCoroutine(InitializeCoroutine());
        }

        private IEnumerator InitializeCoroutine()
        {
            print("SB : Initiating Scenario Manager");
            foreach (var _outline in GetComponentsInChildren<SB_Outline>())
            {
                _outline.enabled = false;
            }

            foreach (var _section in GetComponentsInChildren<Sandbox_Section>())
            {
                yield return new WaitUntil(() => _section.isInit == true);
                sectionList.Add(_section);
            }

            //if (mode == ScenarioModeEnum.LINEAR)
            //{
            foreach (var sobj in GetComponentsInChildren<Sandbox_Object>())
            {
                sobj.linearLock = true;
                print("asd");
            }
            //NextSection();
            //}
            //else
            //{
            //    foreach (var _section in sectionList)
            //        if (_section.invokeStartEventOnSandbox)
            //            _section.onSectionStart.Invoke();
            //}

            isInit = true;

            print("SB : Scenario Manager Initiated");

            yield return new WaitUntil(() => Sandbox_ScenarioUIManager.instance.isInit == true);
            ScreenFade(0, 1f);
        }

        public void StartScenario()
        {
            if (!isStarted)
            {
                isStarted = true;
                if (mode == ScenarioModeEnum.LINEAR)
                {
                    NextSection();
                }
                else
                {
                    print("asd2");
                    foreach (var sobj in GetComponentsInChildren<Sandbox_Object>())
                        sobj.linearLock = false;
                    
                    foreach (var _section in sectionList)
                        if (_section.invokeStartEventOnSandbox)
                            _section.onSectionStart.Invoke();
                }
            }
        }

        public void NextSection()
        {
            StartCoroutine(NextSectionCor());
        }

        IEnumerator NextSectionCor()
        {
            var _index = currentSection != null ? sectionList.IndexOf(currentSection) + 1 : 0;

            if (sectionList.Count > _index)
            {
                if (currentSection)
                    foreach (var sobj in currentSection.objectList)
                        sobj.linearLock = true;

                currentSection = sectionList[_index];
                currentSection.StartSection();

                float _t =
                    Sandbox_ScenarioUIManager.instance.sdCompleteAnimator.GetAnimationLength("SectionCompletePlay");
                Sandbox_ScenarioUIManager.instance.sdCompleteAnimator.Play("SectionCompletePlay");
                yield return new WaitForSeconds(_t);

                foreach (var sobj in currentSection.objectList)
                    sobj.linearLock = false;

                Sandbox_ScenarioUIManager.instance.OpenStepDetail(currentSection);
            }
            else CompleteScenario();
        }

        public void CheckAllSectionCompletion()
        {
            foreach (var section in sectionList)
            {
                if (!section.isCompleted) return;
            }

            CompleteScenario();
        }

        private void CompleteScenario()
        {
            isTimePause = true;

            int _point = 100 / sectionList.Count;
            playScore = _point * sectionList.Count((x) => x.isCorrect);

            //SetupScore();
            Sandbox_ScenarioUIManager.instance.CompleteScenario();
            //SetupScore();
        }

        public void TimeCount()
        {
            if (isTimePause && isInit) return;
            playTime += Time.deltaTime;
        }

        public void ScreenFade(float alpha, float time)
        {
            var finder = FindObjectOfType<HVRGlobalFadeFinder>();
            if (finder)
            {
                var ScreenFader = finder.gameObject.GetComponent<HVRCanvasFade>();
                if (ScreenFader)
                    ScreenFader.CanvasGroup.DOFade(alpha, time);
            }
        }

        public void CheckSectionCorrect(Sandbox_Section section)
        {
            foreach (var _section in sectionList)
            {
                if (_section != section && !_section.isCompleted)
                    break;

                if (_section == section)
                {
                    print("here");
                    section.isCorrect = true;
                    break;
                }
            }
        }

        public void SetupScore(string finalTime)
        {
            /*foreach (Sandbox_Section section in sectionList)
            {
                foreach (ScenarioSectionStep step in section.stepList)
                {
                    
                }
            }*/
            string timeScore =
                PlayerPrefs.GetString(RuntimeManager.Instance.currentSubScenario.SubScenarioName + "\\Time",
                    "--:--|--:--");
            string[] split = timeScore.Split('|');
            switch (mode)
            {
                case ScenarioModeEnum.LINEAR:
                    split[0] = finalTime;
                    break;
                case ScenarioModeEnum.SANDBOX:
                    split[1] = finalTime;
                    break;
            }

            PlayerPrefs.SetString(RuntimeManager.Instance.currentSubScenario.SubScenarioName + "\\Time",
                $"{split[0]}|{split[1]}");

            string scenarioScore =
                PlayerPrefs.GetString(RuntimeManager.Instance.currentSubScenario.SubScenarioName + "\\Play", "--|--");
            split = scenarioScore.Split('|');
            switch (mode)
            {
                case ScenarioModeEnum.LINEAR:
                    split[0] = $"{playScore}";
                    break;
                case ScenarioModeEnum.SANDBOX:
                    split[1] = $"{playScore}";
                    break;
            }

            PlayerPrefs.SetString(RuntimeManager.Instance.currentSubScenario.SubScenarioName + "\\Play",
                $"{split[0]}|{split[1]}");

            _scores.Add(new ScoreValue(DateTime.Now, RuntimeManager.Instance.currentSubScenario.SubScenarioName + " " + (mode == ScenarioModeEnum.LINEAR ? "Latihan" : "Tes"),
                playScore, finalTime, EuRuntimeManager.Instance.Guest.name, EuRuntimeManager.Instance.Guest.email));
            CsvUtil.SaveObjects(_scores, Application.dataPath + "/scores.csv");
        }
    }

    [Serializable]
    public class ScoreValue
    {
        public DateTime dateTime;
        public string playerName;
        public string playerEmail;
        public string course;
        public float score;
        public string playTime;

        public ScoreValue()
        {
        }

        public ScoreValue(DateTime dateTime, string course, float score, string playTime, string playerName,
            string playerEmail)
        {
            this.dateTime = dateTime;
            this.course = course;
            this.score = score;
            this.playTime = playTime;
            this.playerName = playerName;
            this.playerEmail = playerEmail;
        }
    }
}