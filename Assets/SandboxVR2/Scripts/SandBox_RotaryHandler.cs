using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEditor;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Grabbers;
using System.Collections;

namespace SandboxVRSeason2.Framework
{
    public class SandBox_RotaryHandler : Sandbox_ToolSlot
    {
        public string toolId = "";
        public Transform attachPoint = null;
        public Transform axisTransform = null;

        [Space]
        public float rotateAmount = 0f;
        public float detachLimit = 0.3f;

        [FoldoutGroup("Debug")] [SerializeField, ReadOnly] public bool isLocked;
        [FoldoutGroup("Debug")] [SerializeField, ReadOnly] public float currentAttachDelay = 0f;
        [FoldoutGroup("Debug")] [SerializeField] public float attachDelay = 0.3f;
        [FoldoutGroup("Debug")] [SerializeField, ReadOnly] private bool isAttachDelay;

        [FoldoutGroup("Debug")] [SerializeField, ReadOnly] private float rotateValue;
        [FoldoutGroup("Debug")] [SerializeField, ReadOnly] private Sandbox_Tool currentTool;

        private Coroutine _rotaryCoroutine;

        [FoldoutGroup("Gizmo")] [SerializeField] bool drawGizmo = false;
        [FoldoutGroup("Gizmo")] [SerializeField] Vector3 _gizmoCubeSize = new Vector3();
        [FoldoutGroup("Gizmo")] [SerializeField] Vector3 _gizmoCylinderSize = new Vector3();
        [FoldoutGroup("Gizmo")] [SerializeField] Mesh _gizmoCylinderMesh = null;
        [FoldoutGroup("Gizmo")] [SerializeField] Color _gizmoColor = new Color();

        private void Update()
        {
            if (currentTool != null && attachPoint)
            {
                currentTool.grabbable.transform.position = attachPoint.TransformPoint(Vector3.zero);
                currentTool.grabbable.transform.rotation = attachPoint.transform.rotation;
            }

            if (isAttachDelay)
            {
                currentAttachDelay += Time.deltaTime;
                if (currentAttachDelay >= attachDelay)
                    isAttachDelay = false;
            }
        }

        private void Detach(HVRHandGrabber _grabber = null, HVRGrabbable _grabbable = null)
        {
            if (_rotaryCoroutine != null)
            {
                StopCoroutine(_rotaryCoroutine);
                _rotaryCoroutine = null;
            }

            if (currentTool)
                currentTool.isCurrentlyUsed = false;

            currentTool = null;

            currentAttachDelay = 0f;
            isAttachDelay = true;

        }

        public override void SetToolActive(bool value)
        {
            isLocked = !value;
            SetLock();
            
        }

        void SetLock()
        {
            var _lock = isLocked || linearLock ? true : false;
            GetComponent<Rigidbody>().detectCollisions = !_lock;
        }

        private void Complete()
        {
            Detach();

            rotateAmount = rotateAmount * -1;

            ToolComplete?.Invoke();
            rotateValue = 0f;
        }


        private void OnTriggerEnter(Collider other)
        {

            if (isAttachDelay || RotaryFulfilled() || _rotaryCoroutine != null || !other.CompareTag("Scenario Tool"))
                return;

            var _tool = other.GetComponent<Sandbox_Tool>();
            if (_tool == null) return;

            var _grabbable = _tool.grabbable;
            if (_tool.toolId != toolId || _tool.isCurrentlyUsed || !_grabbable.IsHandGrabbed)
                return;

            currentTool = _tool;

            _tool.isCurrentlyUsed = true;
            _grabbable.HandReleased.AddListener(Detach);
            _rotaryCoroutine = StartCoroutine(HandleRotation(_grabbable.HandGrabbers[0].transform));
        }

        float lastRot;
        private IEnumerator HandleRotation(Transform grabberTransform)
        {
            LocalLookAt(grabberTransform);

            yield return null;

            lastRot = GetCurrentAxisRotation();
            float lastRotFrame = lastRot;
            float deltaRotFrame = 0f;

            float tempRot = rotateValue;
            float deltaRot;
            float deltaRotAbs = 0f;

            while (true)
            {
                LocalLookAt(grabberTransform);

                if (Vector3.Distance(axisTransform.transform.position, grabberTransform.position) > detachLimit)
                {
                    Detach();
                }

                yield return null;

                if (Mathf.Abs(GetCurrentAxisRotation() - lastRot) > 30f)
                {
                    lastRot = GetCurrentAxisRotation();
                    tempRot = rotateValue;
                }

                deltaRot = GetCurrentAxisRotation() - lastRot;

                deltaRotFrame = GetCurrentAxisRotation() - lastRotFrame;
                lastRotFrame = GetCurrentAxisRotation();
                if (rotateAmount > 0f ? deltaRotFrame > 0f : deltaRotFrame < 0f)
                    deltaRotAbs += Mathf.Abs(deltaRot);

                rotateValue = rotateAmount > 0 ? Mathf.Clamp(tempRot + deltaRot, -50, rotateAmount + 10) : Mathf.Clamp(tempRot + deltaRot, rotateAmount - 10, 50);

                if (!RotaryFulfilled()) continue;
                Complete();
                yield break;
            }
        }
        private float GetCurrentAxisRotation()
        {
            return axisTransform.transform.localEulerAngles.y;
        }

        private bool RotaryFulfilled()
        {
            if (rotateAmount > 0f && rotateValue >= rotateAmount) return true;
            else if (rotateAmount < 0f && rotateValue <= rotateAmount) return true;
            else return false;
        }
        private void LocalLookAt(Transform grabberTransform)
        {
            float DistanceToPlane = Vector3.Dot(axisTransform.transform.up, grabberTransform.position - axisTransform.transform.position);
            Vector3 plantPoint = grabberTransform.position - axisTransform.transform.up * DistanceToPlane;

            axisTransform.transform.LookAt(plantPoint, axisTransform.transform.up);
        }

        #region Gizmo
        void OnDrawGizmos()
        {
            if (drawGizmo)
            {
                Gizmos.color = _gizmoColor;
                Gizmos.matrix = Matrix4x4.TRS(axisTransform.transform.position, transform.rotation, _gizmoCubeSize);
                Gizmos.DrawCube(Vector3.zero, Vector3.one);
                Gizmos.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);
                Gizmos.DrawMesh(_gizmoCylinderMesh, axisTransform.transform.position, Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z), _gizmoCylinderSize);
            }
        }
        #endregion
        
        public void addValue(float value)
        {
            lastRot -= value;
        }
    }
}