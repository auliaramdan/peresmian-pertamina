using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using HurricaneVR.Framework.Core;

namespace SandboxVRSeason2.Framework
{
    public class SB_Outline : Outline
    {
        [Header("Scenario")]
        [SerializeField] bool animateOutline = true;
        [SerializeField, ShowIf("animateOutline")] float minWidth = 2;
        [SerializeField, ShowIf("animateOutline")] float maxWidth = 5;
        [SerializeField, ShowIf("animateOutline")] float interval = 0.3f;

        [Space]
        [SerializeField]
        HVRGrabbable[] grabbbableList = null;

        Coroutine _animCor = null;
        Color baseColor = Color.green;
        int grabCount = 0;

        private void Start()
        {
            baseColor = OutlineColor;
            foreach (var grabbbable in grabbbableList)
            {
                grabbbable.HandGrabbed.AddListener((x, y) =>
                {
                    grabCount++;
                    OutlineColor = new Color(0, 0, 0, 0);
                });
                grabbbable.HandReleased.AddListener((x, y) =>
                {
                    grabCount--;
                    if (grabCount <= 0)
                        OutlineColor = baseColor;
                });
            }
        }

        private void OnEnable()
        {
            base.OnEnable();
            if (animateOutline)
                _animCor = StartCoroutine(AnimateOutline());
        }

        private void OnDisable()
        {
            base.OnDisable();
            if (_animCor != null)
                StopCoroutine(_animCor);
        }

        private IEnumerator AnimateOutline()
        {
            while (true)
            {
                DOTween.To(() => OutlineWidth, x => OutlineWidth = x, maxWidth, interval);
                yield return new WaitForSeconds(interval);
                DOTween.To(() => OutlineWidth, x => OutlineWidth = x, minWidth, interval);
                yield return new WaitForSeconds(interval);
            }
        }
    }
}
