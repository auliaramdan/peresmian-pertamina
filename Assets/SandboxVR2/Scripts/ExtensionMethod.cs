using SandboxVRSeason2.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using Tictech.LoadManager;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public static class ExtensionMethod
{
    public static void changeValue(this SandBox_RotaryHandler rotary, float value)
    {

    }

    public static float GetAnimationLength(this Animator _animator, string _animationName)
    {
        _animator.Play(_animationName);
        List<AnimationClip> clipList = new List<AnimationClip>(_animator.runtimeAnimatorController.animationClips);

        var _animClip = clipList.Find(x => x.name == _animationName);
        return _animClip.length;

    }

    public static void Spawn<T>(this AssetReference assetReference, Transform parent, Action<T> onComplete)
    {
        ResourceManager.LoadAddressable(assetReference, () =>
        {
            assetReference.InstantiateAsync(parent).Completed += (async) =>
            {
                onComplete.Invoke(async.Result.GetComponent<T>());
            };
        });
    }

    public static void Load<T>(this AssetReference assetReference, Transform parent, Action<T> onComplete)
    {
        ResourceManager.LoadAddressable(assetReference, () =>
        {
            assetReference.LoadAssetAsync<T>().Completed += (async) =>
            {
                onComplete.Invoke(async.Result);
            };
        });
    }

}
