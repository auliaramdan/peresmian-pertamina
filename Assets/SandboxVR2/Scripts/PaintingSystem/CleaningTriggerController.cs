using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace SandboxVRSeason2.Framework
{
    public class CleaningTriggerController : MonoBehaviour
    {
        public float cleanProgress = 0;
        public bool isTriggered;
        public UnityEvent onTriggered;

        public float lerpDuration = 0.5f;

        float endValue = 0.5f;

        [ReadOnly] public bool linearLock;
        private void OnTriggerStay(Collider other)
        {
            if (linearLock) return;
            if (other.tag == "Clean Trigger")
            {
                if (cleanProgress <= endValue)
                {
                    cleanProgress += 1 * Time.deltaTime;
                }

                CollisionPainter c = other.GetComponent<CollisionPainter>();
                if (c.currentBrushMode == CollisionPainter.BrushMode.Cleaning)
                {
                    c.strength = cleanProgress;
                }

                if (cleanProgress >= endValue) Trigger();
            }
        }

        public void Trigger()
        {
            isTriggered = true;
            onTriggered?.Invoke();

            if (GetComponentInParent<CleaningTriggerGroupController>())
                GetComponentInParent<CleaningTriggerGroupController>().CheckAllTriggers();
        }
    }
}
