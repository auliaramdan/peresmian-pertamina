using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace SandboxVRSeason2.Framework
{
    public class CleaningTriggerGroupController : Sandbox_Object
    {
        public List<CleaningTriggerController> triggers;
        public UnityEvent onAnyTriggered;
        public UnityEvent onAllTriggered;

        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _linearLock;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _init;

        void Start()
        {
            if (triggers.Count == 0)
            {
                foreach (Transform child in transform)
                {
                    triggers.Add(child.GetComponent<CleaningTriggerController>());
                }
            }
            _init = true;
        }

        public void CheckAllTriggers()
        {
            int count = 0;
            for (int i = 0; i < triggers.Count; i++)
            {
                if (triggers[i].isTriggered)
                {
                    count++;
                }
            }

            if (count > 0)
            {
                onAnyTriggered?.Invoke();
                //Debug.Log("ONE of the paint triggers is triggered");
            }
            if (count == triggers.Count)
            {
                onAllTriggered?.Invoke();
                //Debug.Log("ALL of the paint triggers is triggered");
            }
        }

        public override bool linearLock
        {
            get { return _linearLock; }
            set
            {
                if (!_linearException)// && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _linearLock = value;
                    StartCoroutine(SetLock(value)); 
                    if (Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }

        IEnumerator SetLock(bool _value)
        {
            yield return new WaitUntil(() => _init);
            SetLockOnTrigger(_value);
        }

        void SetLockOnTrigger(bool _value)
        {
            foreach (var _trigger in triggers)
            {
                _trigger.linearLock = _value;
            }
        }
    }
}
