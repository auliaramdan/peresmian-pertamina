using System.Collections.Generic;
using DG.Tweening;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Grabbers;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;


namespace SandboxVRSeason2.Framework
{
    public class Sandbox_Tool : MonoBehaviour
    {
        public string toolId;

        private bool _isCurrentlyUsed;
        public bool isCurrentlyUsed
        {
            get { return _isCurrentlyUsed; }
            set
            {
                _isCurrentlyUsed = value;
                if (value)
                {
                    print("tool used");
                    onUsed.Invoke();
                }
                else
                {
                    print("tool unused");
                    onUnused.Invoke();
                }
            }
        }
        public HVRGrabbable grabbable;

        [FoldoutGroup("Event")] public UnityEvent onUsed;
        [FoldoutGroup("Event")] public UnityEvent onUnused;

        protected void Start()
        {
            gameObject.tag = "Scenario Tool";
            if (!grabbable) grabbable = GetComponent<HVRGrabbable>();
        }
    }
}
