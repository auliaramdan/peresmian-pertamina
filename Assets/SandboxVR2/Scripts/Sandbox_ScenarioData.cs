using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SandboxVRSeason2.Framework
{
    [CreateAssetMenu(fileName = "New Scenario Data", menuName = "Sandbox/Scenario Data", order = 0)]
    public class Sandbox_ScenarioData : ScriptableObject
    {
        public string ScenarioName;
        public Sprite ScenarioSprite;
        public Sandbox_SubScenarioData[] SubScenariolist;
    }
}
