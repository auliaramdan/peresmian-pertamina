using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;


[RequireComponent(typeof(CanvasGroup))]
public class SB_CanvasFade : MonoBehaviour
{
    Tween currentTween;
    Coroutine currentCoroutine;
    [SerializeField] private float fadeTime = 0.3f;
    [SerializeField] private bool isDelayOnOut = false;
    [SerializeField] [ShowIf("isDelayOnOut")] private float fadeOutDelay = 1f;

    bool isActive;

    public void SetShow(bool value)
    {
        currentTween.Kill();
        StopCoroutine(currentCoroutine);
        currentCoroutine = StartCoroutine(SetShowCoroutine(value));
    }
    private IEnumerator SetShowCoroutine(bool value)
    {
        if (value == isActive) yield break;
        isActive = value;

        if (!value && isDelayOnOut)
            yield return new WaitForSeconds(fadeOutDelay);

        currentTween = GetComponent<CanvasGroup>().DOFade(value ? 1 : 0, fadeTime);
    }
}
