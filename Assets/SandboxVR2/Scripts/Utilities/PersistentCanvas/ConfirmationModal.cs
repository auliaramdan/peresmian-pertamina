﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Tictech.Utilities.PersistentCanvas
{
    public class ConfirmationModal : MonoBehaviour, IModal
    {
        public TextMeshProUGUI title;
        public TextMeshProUGUI detail;
        public Button actionButton;
        public Button cancelButton;

        private void Start()
        {
            actionButton.onClick.AddListener(SelfDestruct);
            cancelButton.onClick.AddListener(SelfDestruct);
        }

        public void InitiateParameters(UnityAction action, params string[] list)
        {
            title.text = list[0];
            detail.text = list[1];
            actionButton.GetComponentInChildren<TextMeshProUGUI>().text = list[2];
            cancelButton.GetComponentInChildren<TextMeshProUGUI>().text = list[3];
            
            actionButton.onClick.AddListener(action);
        }

        private void SelfDestruct()
        {
            Destroy(gameObject);
        }
    }
}
