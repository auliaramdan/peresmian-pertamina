﻿using System;

namespace Tictech.EnterpriseUniversity
{
    [Serializable]
    public struct EuUserLogin
    {
        public string username;
        public string password;

        public EuUserLogin(string un, string pw)
        {
            username = un;
            password = pw;
        }
    }
    
    [Serializable]
    public struct EuGuestLogin
    {
        public string email;
        public string phone;

        public EuGuestLogin(string un, string pw)
        {
            email = un;
            phone = pw;
        }
    }
    
    [Serializable]
    public class EuUserData
    {
        public int id;
        public string username;
        public string password;
        public string token;
        public string first_name;
        public object last_name;
        public object panggilan;
        public DateTime tanggal_lahir;
        public object profile_picture;
        public string gender;
        public object phone_number;
        public object nik;
        public object labour_cost;
        public object rfid_access;
        public object fingerprint_access;
        public object jobdescId;
        public object teamId;
        
        public EuUserData(string un, string pw)
        {
            username = un;
            password = pw;
        }

        public EuUserData(string tk)
        {
            token = tk;
        }
    }

    [Serializable]
    public class EuToyotaGuestData
    {
        public int id;
        public string name;
        public string email;
        public string phone;
    }

    [Serializable]
    public class EuToyotaUserData
    {
        public int id;
        public string email;
        public string idNumber;
        public string idToken;
        public EuToyotaUserProfileData profile;

        public EuToyotaUserData(string token)
        {
            idToken = token;
        }
    }

    [Serializable]
    public class EuToyotaUserProfileData
    {
        public int id;
        public int usersId;
        public string fullName;
        public string phoneNumber;
    }

    [Serializable]
    public class EuToyotaUserScoresResponse
    {
        public string status;
        public EuToyotaUserScores[] data;
    }

    [Serializable]
    public class EuToyotaUserScores
    {
        public int trainingId;
        public int score;
    }
    
    [Serializable]
    public class EuResponseData
    {
        public string message;
        public string token;
        public EuUserData GetUser()
        {
            return new EuUserData(token);
        }
    }

    [Serializable]
    public class EuToyotaLoginResponse
    {
        public string status;
        public EuToyotaSignInResponseData data;
    }

    [Serializable]
    public class EuToyotaSignInResponseData
    {
        public string token;
    }

    [Serializable]
    public class EuToyotaUserResponse
    {
        public string status;
        public EuToyotaUserData data;
    }
    
    [Serializable]
    public class EuResponse
    {
        public string status;
        public EuResponseData data;
    }

    [Serializable]
    public struct EuTrainingScore
    {
        public int score;
        public string user_id;
        public string training_id;

        public EuTrainingScore(int sc, string tr, string mbr)
        {
            score = sc;
            user_id = mbr;
            training_id = tr;
        }
    }

    public struct EuTrainingScoreToken
    {
        public int score;
        public int training_id;

        public EuTrainingScoreToken(int sc, int tr)
        {
            score = sc;
            training_id = tr;
        }
    }
}
