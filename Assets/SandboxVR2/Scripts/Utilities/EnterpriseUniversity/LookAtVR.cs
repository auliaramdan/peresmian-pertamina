using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtVR : MonoBehaviour
{

    Transform target;
    private void Start()
    {
        target = GameObject.FindGameObjectsWithTag("LookTarget")[0].transform;
    }

    private void Update()
    {
        transform.LookAt(target);
    }
}
