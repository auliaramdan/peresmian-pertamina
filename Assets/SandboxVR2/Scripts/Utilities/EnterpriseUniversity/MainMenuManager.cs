using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Tictech.LoadManager;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

namespace SandboxVRSeason2.Framework
{
    public class MainMenuManager : MonoBehaviour
    {
        [SerializeField] List<Sandbox_ScenarioData> scenarioList;
        
        [Space]
        [SerializeField] GameObject scenarioListPanel;
        [SerializeField] GameObject subScenarioListPanel;
        [SerializeField] GameObject scenarioDetailPanel;


        [Space]
        [SerializeField] Transform scenariolistParent;
        [SerializeField] SB_UIReference scenarioListPrefab;

        [Space]
        [SerializeField] Transform subScenariolistParent;
        [SerializeField] SB_UIReference subScenarioListPrefab;

        [Space]
        [SerializeField] TextMeshProUGUI scenarioDetailTitleText;
        [SerializeField] Image scenarioDetailImage;
        [SerializeField] SB_UIButton scenarioDetailStartSandbox;
        [SerializeField] SB_UIButton scenarioDetailStartLinear;
        [SerializeField] private TextMeshProUGUI linearScore;
        [SerializeField] private TextMeshProUGUI sandboxScore;
        [SerializeField] private TextMeshProUGUI linearPlayScore;
        [SerializeField] private TextMeshProUGUI sandboxPlayScore;

        [Space] 
        [SerializeField] private Sandbox_SubScenarioData tutorialData;
        [SerializeField] private SB_UIButton tutorialButton;

        [Space] 
        [SerializeField] private Sandbox_SubScenarioData vr360Data;
        [SerializeField] private SB_UIButton vr36DataButton;

        Sandbox_SubScenarioData targetSubsScenario;

        private void Start()
        {
            scenarioListPanel.SetActive(true);
            subScenarioListPanel.SetActive(false);
            scenarioDetailPanel.SetActive(false);

            SetUpScenarioList();

            scenarioDetailStartSandbox.onDown.AddListener(() => { LoadScenario(false); });
            scenarioDetailStartLinear.onDown.AddListener(() => { LoadScenario(true); });
            
            tutorialButton.onDown.AddListener(() => {RuntimeManager.LoadScene(tutorialData.SubScenarioScene);});
            vr36DataButton.onDown.AddListener(() => {RuntimeManager.LoadScene(vr360Data.SubScenarioScene);});
        }

        void SetUpScenarioList()
        {
            foreach (Transform child in scenariolistParent)
                Destroy(child.gameObject);

            foreach (var scenarioData in scenarioList)
            {
                var _bt = Instantiate(scenarioListPrefab, scenariolistParent);
                _bt.imageList[0].sprite = scenarioData.ScenarioSprite;
                _bt.tmProList[0].text = scenarioData.ScenarioName;
                _bt.customButtonList[0].onClick.AddListener(() => { SetUpSubScenarioList(scenarioData); scenarioListPanel.SetActive(false); subScenarioListPanel.SetActive(true); });
            }
        }

        void SetUpSubScenarioList(Sandbox_ScenarioData scenario)
        {
            foreach(Transform child in subScenariolistParent)
                Destroy(child.gameObject);

            foreach (var subScenarioData in scenario.SubScenariolist)
            {
                var _bt = Instantiate(subScenarioListPrefab, subScenariolistParent);
                _bt.imageList[0].sprite = subScenarioData.SubScenarioSprite;
                _bt.tmProList[0].text = subScenarioData.SubScenarioName;
                _bt.customButtonList[0].onDown.AddListener(() => { SetUpScenarioDetails(subScenarioData); subScenarioListPanel.SetActive(false); scenarioDetailPanel.SetActive(true); });
            }
        }

        void SetUpScenarioDetails(Sandbox_SubScenarioData subScenarioData)
        {
            scenarioDetailTitleText.text = subScenarioData.SubScenarioName;
            scenarioDetailImage.sprite = subScenarioData.SubScenarioSprite;

            scenarioDetailStartSandbox.gameObject.SetActive(subScenarioData.isSandbox ? true : false);
            scenarioDetailStartLinear.gameObject.SetActive(subScenarioData.isLinear ? true : false);

            targetSubsScenario = subScenarioData;
            SetupScores();
        }

        void LoadScenario(bool isLinear)
        {
            RuntimeManager.LoadScenario(targetSubsScenario, isLinear);
        }

        void SetupScores()
        {
            string timeScore = PlayerPrefs.GetString(targetSubsScenario.SubScenarioName + "\\Time", "--:--|--:--");
            string[] scores = timeScore.Split('|');
            linearScore.text = scores[0];
            sandboxScore.text = scores[1];
            
            string playScore = PlayerPrefs.GetString(targetSubsScenario.SubScenarioName + "\\Play", "--|--");
            scores = playScore.Split('|');
            linearPlayScore.text = scores[0];
            sandboxPlayScore.text = scores[1];
        }

        public void ResetSessionScores()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}
