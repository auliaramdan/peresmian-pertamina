﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using Sirenix.OdinInspector;
//using Tictech.Utilities.PersistentCanvas;
using UnityEngine;
using UnityEngine.Networking;
//using Tictech.Oculus.Scenario;

namespace Tictech.EnterpriseUniversity
{
    public class EuRuntimeManager : MonoBehaviour
    {
        public static EuRuntimeManager Instance;

        [Header("API Access")]
        public string baseUrl = "https://toyota.clev.co.id/api/v1/";
        public string loginHandle;
        public string usersHandle;
        public string scoreHandle = "training/score-VR";
        public string scoreUserParameter;

        private string _userLink = "user";
        private string _token = "";
        private string _guestSignInLink = "http://toyota-staging.machinevision.global:2020/guest-users";

#if UNITY_EDITOR
        [Header("Debug")]
        public string debugUsername;
        public string debugPassword;
        public int debugTrainingId;
        public int debugScore;

        [Button]
        void TestPostScore()
        {
            Timing.RunCoroutine(PostScore(debugTrainingId, debugScore));
        }

        [Button]
        void TestGetScores()
        {
            Timing.RunCoroutine(GetScores());
        }

        [Button]
        void TestGetGuest()
        {
            Timing.RunCoroutine(GetGuest(1));
        }

        [Button]
        void TestPostGuest()
        {
            Timing.RunCoroutine(SignGuestIn("Lanius@parkiran", "08219123"));
        }

        void TestLogin()
        {
            Timing.RunCoroutine(SignIn(debugUsername, debugPassword));
        }
        #endif

        [ReadOnly]
        public EuToyotaUserData User { get; private set; }
        
        [ReadOnly]
        public EuToyotaUserScores[] Scores { get; private set; }
        
        [ReadOnly]
        public EuToyotaGuestData Guest { get; private set; }
    
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        public IEnumerator<float> SignIn(string username, string password)
        {
            EuToyotaLoginResponse authStatus = null;

            var userLogin = new EuUserLogin(username, password);
            var userBytes =
                System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(userLogin));

            var auth = UnityWebRequest.Put(baseUrl + loginHandle, userBytes);
            auth.method = UnityWebRequest.kHttpVerbPOST;
            auth.SetRequestHeader("Content-Type", "application/json");
            auth.SetRequestHeader("Accept", "application/json");
            Debug.Log("Signing in to: " + auth.url);
            var async = auth.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (auth.isNetworkError || auth.isHttpError)
            {
                Debug.LogError(auth.error);
            }
            else
            {
                Debug.Log(auth.downloadHandler.text);
                authStatus = JsonUtility.FromJson<EuToyotaLoginResponse>(auth.downloadHandler.text);
            }
            
            if (authStatus == null)
            {
                User = null;
                yield return Timing.WaitForSeconds(1f);
            }
            else
            {
                _token = authStatus.data.token;
                Timing.WaitUntilDone(GetUser(_token));
                yield return Timing.WaitForSeconds(1f);
            }
        }
        
        public IEnumerator<float> SignGuestIn(string email, string phone)
        {
            var userLogin = new EuGuestLogin(email, phone);
            var userBytes =
                System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(userLogin));

            var auth = UnityWebRequest.Put(_guestSignInLink, userBytes);
            auth.method = UnityWebRequest.kHttpVerbPOST;
            auth.SetRequestHeader("Content-Type", "application/json");
            auth.SetRequestHeader("Accept", "application/json");
            Debug.Log("Signing in to: " + auth.url);
            var async = auth.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (auth.isNetworkError || auth.isHttpError)
            {
                Debug.LogError(auth.error);
            }
            else
            {
                Debug.Log(auth.downloadHandler.text);
                Guest = JsonUtility.FromJson<EuToyotaGuestData>(auth.downloadHandler.text);
            }
        }

        public IEnumerator<float> PostScore(int trainingId, int score)
        {
#if UNITY_EDITOR
            if (User == null || string.IsNullOrEmpty(_token))
                yield return Timing.WaitUntilDone(SignIn(debugUsername, debugPassword));
#endif
            if (User == null)
                yield break;
            if (string.IsNullOrEmpty(_token))
            {
                Debug.LogWarning("User isn't signed in, failed to post score.");
                yield break;
            }
            else
            {
                Debug.Log("Uploading score to:\r\ntraining_id: " + trainingId + "\r\nscore: " + score + "\r\nbearer: " + _token);
            }

            var trainingData = JsonUtility.ToJson(new EuTrainingScoreToken(score, trainingId));
            trainingData = trainingData.Replace("username\":", scoreUserParameter + "\":");
            var post = UnityWebRequest.Put(baseUrl + scoreHandle, System.Text.Encoding.UTF8.GetBytes(trainingData));
            Debug.Log(post.url);
            post.method = UnityWebRequest.kHttpVerbPOST;
            post.SetRequestHeader("Content-Type", "application/json");
            post.SetRequestHeader("Accept", "application/json");
            post.SetRequestHeader("Authorization", "Bearer " + _token);
            post.uploadHandler.contentType = "application/json";
            var async = post.SendWebRequest();
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }
            
            if (post.isNetworkError || post.isHttpError)
            {
                Debug.LogError(post.error);
            }
            else
            {
                Debug.Log(post.downloadHandler.text);
                Debug.Log("Score submitted. response: " + post.downloadHandler.text);
            }
        }

        public IEnumerator<float> GetUser(string token)
        {
            EuToyotaUserResponse authStatus = null;
            
            var request = UnityWebRequest.Get(baseUrl + _userLink);
            request.method = UnityWebRequest.kHttpVerbGET;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + token);
            var async = request.SendWebRequest();
            
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                authStatus = JsonUtility.FromJson<EuToyotaUserResponse>(request.downloadHandler.text);
                User = authStatus.data;
                Debug.Log(User.email);
            }
        }
        
        public IEnumerator<float> GetGuest(int id)
        {
            var request = UnityWebRequest.Get($"{_guestSignInLink}/{id}");
            request.method = UnityWebRequest.kHttpVerbGET;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            var async = request.SendWebRequest();
            
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                Guest = JsonUtility.FromJson<EuToyotaGuestData>(request.downloadHandler.text);
                Debug.Log(Guest.email);
            }
        }

        public IEnumerator<float> GetScores()
        {
            EuToyotaUserScoresResponse scoreResponse = null;
            
            var request = UnityWebRequest.Get(baseUrl + "training/user-VR");
            request.method = UnityWebRequest.kHttpVerbGET;
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            request.SetRequestHeader("Authorization", "Bearer " + _token);
            var async = request.SendWebRequest();
            
            while (!async.isDone)
            {
                yield return Timing.WaitForOneFrame;
            }

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                scoreResponse = JsonUtility.FromJson<EuToyotaUserScoresResponse>(request.downloadHandler.text);
                Scores = scoreResponse.data;
            }
        }

        public void PertaminaGuestUser(string guestName, string email)
        {
            Guest = new EuToyotaGuestData
            {
                name = guestName,
                email = email
            };
        }

        public void Logout()
        {
            User = null;
            Scores = null;
            Guest = null;
        }
    }
}