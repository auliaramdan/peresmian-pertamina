using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Shared;

namespace SandboxVRSeason2.Framework
{
    public class SB_ControllerInput : MonoBehaviour
    {
        [SerializeField] bool needGrabbable;
        [SerializeField, HideIf("needGrabbable")] HVRHandSide handSide;
        [SerializeField, ShowIf("needGrabbable")] HVRGrabbable grabbable;
        [SerializeField] InputEvent[] inputEvent;

        private void Update()
        {
            if (needGrabbable && (!grabbable || !grabbable.IsHandGrabbed)) return;

            //print("ret terpegang");

            foreach (var inputEvent in inputEvent)
            {
                if (needGrabbable)
                {
                    var buttonState = HVRController.GetButtonState(grabbable.HandGrabbers[0].HandSide == HVRHandSide.Left ? HVRHandSide.Left : HVRHandSide.Right, inputEvent.input);

                    //print("ret trigger");
                    if (buttonState.JustActivated) inputEvent.OnActive.Invoke();
                    if (buttonState.JustDeactivated) inputEvent.OnDeactive.Invoke();
                    if (buttonState.Active) inputEvent.WhileActive.Invoke();
                }
                else
                {
                    var buttonState = HVRController.GetButtonState(handSide, inputEvent.input);

                    if (buttonState.JustActivated) inputEvent.OnActive.Invoke();
                    if (buttonState.JustDeactivated) inputEvent.OnDeactive.Invoke();
                    if (buttonState.Active) inputEvent.WhileActive.Invoke();
                }
            }
        }

        [System.Serializable]
        class InputEvent
        {
            public HVRButtons input = HVRButtons.Trigger;

            [FoldoutGroup("Event")] public UnityEvent OnActive;
            [FoldoutGroup("Event")] public UnityEvent OnDeactive;
            [FoldoutGroup("Event")] public UnityEvent WhileActive;

        }
    }
}