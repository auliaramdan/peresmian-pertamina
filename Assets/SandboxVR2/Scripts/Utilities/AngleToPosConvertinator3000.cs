using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class AngleToPosConvertinator3000 : MonoBehaviour
{
    [SerializeField] Transform target;

    [Space]
    [SerializeField] float currentValue;
    [SerializeField] float minValue;
    [SerializeField] float maxValue;

    [FoldoutGroup("Setting"), SerializeField] bool xPos;
    [HorizontalGroup("Setting/HorizontalX", LabelWidth = 50)]
    [FoldoutGroup("Setting"), BoxGroup("Setting/HorizontalX/One", showLabel: false), SerializeField, ShowIf("xPos")] float xMin;
    [FoldoutGroup("Setting"), BoxGroup("Setting/HorizontalX/Two", showLabel: false), SerializeField, ShowIf("xPos")] float xMax;

    [FoldoutGroup("Setting"), SerializeField] bool yPos;
    [HorizontalGroup("Setting/HorizontalY", LabelWidth = 50)]
    [FoldoutGroup("Setting"), BoxGroup("Setting/HorizontalY/One", showLabel: false), SerializeField, ShowIf("yPos")] float yMin;
    [FoldoutGroup("Setting"), BoxGroup("Setting/HorizontalY/Two", showLabel: false), SerializeField, ShowIf("yPos")] float yMax;

    [FoldoutGroup("Setting"), SerializeField] bool zPos;
    [HorizontalGroup("Setting/HorizontalZ", LabelWidth = 50)]
    [FoldoutGroup("Setting"), BoxGroup("Setting/HorizontalZ/One", showLabel: false), SerializeField, ShowIf("zPos")] float zMin;
    [FoldoutGroup("Setting"), BoxGroup("Setting/HorizontalZ/Two", showLabel: false), SerializeField, ShowIf("zPos")] float zMax;




    private void Update()
    {
    }


    public void OnAngleChanged(float angle)
    {
        currentValue = angle;
        var _scaledValue = scaledValue(currentValue, minValue, maxValue);
        //print("popo" + angle + " " + _scaledValue);

        Vector3 newPos = target.localPosition;

        if (xPos)
        {
            var xRange = xMax - xMin;
            var xScaled = xRange * _scaledValue;
            newPos.x = xMin + xScaled;
        }
        if (yPos)
        {
            var yRange = yMax - yMin;
            var yScaled = yRange * _scaledValue;
            newPos.y = yMin + yScaled;
        }
        if (zPos)
        {
            var zRange = zMax - zMin;
            var zScaled = zRange * _scaledValue;
            newPos.z = xMin + zScaled;
        }

        target.localPosition = newPos;
    }

    float scaledValue(float _rawValue, float _minValue, float _maxValue)
    {
        var clamped = Mathf.Clamp(_rawValue, _minValue, _maxValue);
        var _scaledValue = (clamped - _minValue) / (_maxValue - _minValue);
        return _scaledValue;
    }
}
