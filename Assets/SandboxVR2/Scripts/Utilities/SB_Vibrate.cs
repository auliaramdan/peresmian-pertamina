using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SB_Vibrate : MonoBehaviour
{
    public bool CanVibrate { get; set; } = false;
    
    private enum HandSide
    {
        LEFT, RIGHT, BOTH
    }

    [SerializeField] HandSide handSide = HandSide.BOTH;
    [SerializeField] float duration = 1;
    [SerializeField] float amplitude = 0.5f;

    private HVRController leftController;
    private HVRController rightController;

    private void Start()
    {
        leftController = HVRInputManager.Instance.LeftController;
        rightController = HVRInputManager.Instance.RightController;
    }

    public void Vibrate()
    {
        if (!CanVibrate)
            return;
        
        if(handSide == HandSide.BOTH || handSide == HandSide.LEFT)
            leftController.Vibrate(amplitude, duration);

        if (handSide == HandSide.BOTH || handSide == HandSide.RIGHT)
            rightController.Vibrate(amplitude, duration);
    }

    public void StopVibrate()
    {
        if(handSide == HandSide.BOTH || handSide == HandSide.LEFT)
            leftController.Vibrate(0);

        if (handSide == HandSide.BOTH || handSide == HandSide.RIGHT)
            rightController.Vibrate(0);
    }
}
