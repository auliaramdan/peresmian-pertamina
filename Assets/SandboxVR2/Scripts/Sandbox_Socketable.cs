﻿using HurricaneVR.Framework.Core.Sockets;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_Socketable : HVRSocketable
    {
        public Sandbox_ObjectProfile objectProfile;
    }
}