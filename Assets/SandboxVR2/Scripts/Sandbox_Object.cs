using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace SandboxVRSeason2.Framework
{
    public abstract class Sandbox_Object : MonoBehaviour
    {
        [PropertyOrder(2), FoldoutGroup("Debug"), SerializeField]
        public bool _linearException;
        public abstract bool linearLock { get; set; }
        [PropertyOrder(2), FoldoutGroup("Debug"), SerializeField] public List<SB_Outline> outlineList;
    }
}
