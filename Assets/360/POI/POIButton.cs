using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum POIType {CompanyProfile, Contact, LiveEvents, ProductCatalog, Website, Gacha, PhotoBooth}

public class POIButton : MonoBehaviour
{
    public bool Enable
    {
        get
        {
            return enable;
        }
        set
        {
            enable = value;
            gameObject.SetActive(value);
        }
    }

    public Vector3 worldPosition;

    bool enable = true;
    public Button button;
    public TMP_Text buttonName;

    public TMP_Text[] textFields;
    public Image[] imageFields;

    #region PUBLIC_METHODS
    public void SetPOIButtonOnClick(UnityEngine.Events.UnityAction action)
    {
        button.onClick.AddListener(action);
    }
    #endregion
}
