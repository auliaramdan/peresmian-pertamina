using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void onCameraRotate();

public class CameraControl : MonoBehaviour
{
    public static CameraControl Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<CameraControl>(true);
            }
            return instance;
        }
    }

    public static onCameraRotate OnCameraRotate;

    public Camera minimapCamera;
    public float rotateSpeed;
    public float xRange;

    bool isPressed = false;
    public bool canRotate = true;

    private static CameraControl instance;

    public Vector3 rotationTrack;
    Vector3 lastCursorPosition;
    Vector3 delta;

    private void Awake()
    {
        OnCameraRotate += Rotate;
    }

    private void OnDisable()
    {
        isPressed = false;
        lastCursorPosition = Vector3.zero;
    }

    private void Start()
    {
        rotationTrack = transform.eulerAngles;
    }

    private void Update()
    {
        if(Input.GetMouseButton(0))
        {
            if (canRotate)
            {
                OnCameraRotate();
            }
        }
        if(Input.GetMouseButtonUp(0))
        {
            lastCursorPosition = Vector3.zero;
            isPressed = false;

            //HDRIVixbitBehaviour.Instance.enabled = true;
        }
        if (Input.GetMouseButtonDown(0))
        {
            lastCursorPosition = Input.mousePosition;
            isPressed = true;
        }
    }

    public static void Initialize(Vector3 localEulerAngles)
    {
        instance.rotationTrack = localEulerAngles;
        instance.transform.localEulerAngles = localEulerAngles;
    }

    void Rotate()
    {
        if (isPressed == true)
        {
            delta = lastCursorPosition - Input.mousePosition;

            /*if(delta != Vector3.zero)
            {
                HDRIVixbitBehaviour.Instance.enabled = false;
            }*/

            rotationTrack.y += delta.x * rotateSpeed * Time.fixedDeltaTime;
            rotationTrack.x -= delta.y * rotateSpeed * Time.fixedDeltaTime;
            rotationTrack.x = Mathf.Clamp(rotationTrack.x, -xRange, xRange);

            transform.eulerAngles = rotationTrack;
        }
        lastCursorPosition = Input.mousePosition;

        //Vector3 minimapEuler = Vector3.zero;
        //minimapEuler.y = rotationTrack.y;
        //minimapEuler.x = minimapCamera.transform.localEulerAngles.x;
        //minimapCamera.transform.localEulerAngles = minimapEuler;
    }
}
