using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class View360Manager : MonoBehaviour
{
    public static View360Manager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<View360Manager>(true);
            }
            return instance;
        }
    }

    public int startPoint;

    [Space(5f)]
    public bool useRemoteTextures;
    public float defaultRot;

    public TeleportPoint pointAtStart;
    public Transform sphereTransform;
    public Transform teleportPointParent;
    public Material HDRIMaterial;

    public List<TeleportPoint> teleportPoints = new List<TeleportPoint>();

    public List<GameObject> objectToDisable = new List<GameObject>();
    public List<GameObject> objectToDisableAtStart = new List<GameObject>();
    private static View360Manager instance;

    #region MONOBEHAVIOUR
    private void Start()
    {
        Initialize();
        if (useRemoteTextures)
        {
            SetRemoteURLs();
        }

        foreach(var o in objectToDisableAtStart)
        {
            o.SetActive(false);
        }

        RenderSettings.skybox = HDRIMaterial;
    }

    private void Update()
    {
        foreach (var o in objectToDisable)
        {
            o.SetActive(false);
        }
    }
    #endregion

    #region INSTANCE_METHOD
    private void Initialize()
    {
        teleportPoints.AddRange(FindObjectsOfType<TeleportPoint>().OrderBy(o=>o.teleportID));
        VRHDRITeleportBehaviour.Teleport(pointAtStart, null);
    }

    void SetRemoteURLs()
    {

    }
    #endregion

    #region STATIC_METHOD
    public static void SetControl(bool value)
    {
        VRHDRITeleportBehaviour.Instance.enabled = value;
        //CameraControl.Instance.enabled = value;
    }

    public static void SetHDRI(Texture HDRI)
    {
        instance.HDRIMaterial.mainTexture = HDRI;
    }

    public static void SetRotation(float value)
    {
        Vector3 eulerY = Vector3.up;
        eulerY *= value + instance.defaultRot;
        eulerY.x = 0f;
        instance.sphereTransform.localEulerAngles = eulerY;
    }
    #endregion
}