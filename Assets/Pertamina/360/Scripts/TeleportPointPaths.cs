using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Pathway
{
    public TeleportPoint startPoint;
    public TeleportPoint endPoint;
}

public class TeleportPointPaths : MonoBehaviour
{
    public static TeleportPointPaths Instance
    {
        get
        {
            if(instance==null)
            {
                instance = FindObjectOfType<TeleportPointPaths>(true);
            }
            return instance;
        }
    }

    public Transform pathwayParents;
    public GameObject pathwayPrefab;

    [SerializeField]
    public List<Pathway> pathways = new List<Pathway>();

    private static TeleportPointPaths instance;

    private void Start()
    {
        LineRenderer lr;

        foreach(var p in pathways)
        {
            lr = Instantiate(pathwayPrefab.transform, p.startPoint.transform.position, Quaternion.Euler(90f,0f,0f), pathwayParents).GetComponent<LineRenderer>();
            lr.gameObject.name = "Pathway" + p.startPoint.name + p.endPoint.name;

            lr.positionCount = 2;
            lr.SetPosition(0, p.startPoint.transform.position);
            lr.SetPosition(1, p.endPoint.transform.position);
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        foreach(var p in pathways)
        {
            if (p.startPoint != null && p.endPoint != null)
            {
                Gizmos.DrawLine(p.startPoint.transform.position, p.endPoint.transform.position);
            }
        }
    }
}
