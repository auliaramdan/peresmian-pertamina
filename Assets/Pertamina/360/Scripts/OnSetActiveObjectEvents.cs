using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnGameObjectActivated : UnityEvent
{

}

[System.Serializable]
public class OnGameObjectDeactivated : UnityEvent
{
    
}

public class OnSetActiveObjectEvents : MonoBehaviour
{
    public OnGameObjectActivated onGameObjectActivated;
    public OnGameObjectDeactivated onGameObjectDeactivated;

    private void OnEnable()
    {
        onGameObjectActivated.Invoke();
    }

    private void OnDisable() {
        Invoke("InvokeDisable", 1f);
    }

    void InvokeEnable()
    {
        
    }

    void InvokeDisable()
    {
        onGameObjectDeactivated.Invoke();
    }
}
