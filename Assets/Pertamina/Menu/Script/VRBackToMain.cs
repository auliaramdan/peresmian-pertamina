using HurricaneVR.Framework.ControllerInput;
using HurricaneVR.Framework.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRBackToMain : MonoBehaviour
{
    public Transform mainCamera;

    HVRController controller;
    
    public void Start()
    {
        controller = HVRInputManager.Instance.LeftController;
    }
}
