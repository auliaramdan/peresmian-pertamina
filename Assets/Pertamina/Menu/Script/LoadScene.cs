using HurricaneVR.Framework.Core.UI;
using System.Collections;
using System.Collections.Generic;
using Tictech.LoadManager;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public Transform parentObj;
    public List<Canvas> canvasesToDisable;

    static int currentSceneID;

    private void Start()
    {
        if(parentObj != null)
            parentObj.gameObject.SetActive(false);
    }

    public static void LoadSceneAdditive(int sceneID)
    {
        RuntimeManager.LoadScene(RuntimeManager.Instance.defaultScene);
    }

    private void Update()
    {
        if(canvasesToDisable.Count>0)
        {
            foreach (var c in canvasesToDisable)
            {
                c.gameObject.SetActive(false);
            }
        }
    }

    public void QuitGame(){
        Application.Quit();
    }
}
