using HurricaneVR.Framework.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using HurricaneVR.Framework.Core;
using HurricaneVR.Framework.Core.Grabbers;
using UnityEngine.UI;
using System.Linq;

namespace SandboxVRSeason2.Framework
{
    public class Sandbox_ValveControllerMK2 : Sandbox_Object
    {
        [ShowIf("isplaying"), ReadOnly, SerializeField] private int _valveMinAngle;
        [ShowIf("isplaying"), ReadOnly, SerializeField] private int _valveMaxAngle;

        [HideIf("isplaying")] public int valveMinAngle = 0;
        [HideIf("isplaying")] public int valveMaxAngle;
        [HideIf("isplaying")] public int valveStartAngle;

        [FoldoutGroup("Event")] public Sandbox_ValveEvent[] valveEvent;

        [Space(3)]
        [FoldoutGroup("Event")] public UnityEvent onHandGrabbed;
        [FoldoutGroup("Event")] public UnityEvent onHandReleased;
        [FoldoutGroup("Event")] public UnityEvent<float> onAngleChanged;

        [FoldoutGroup("Indicator")] public bool useIndicator = true;
        [FoldoutGroup("Indicator")] public GameObject indicatorPrefab;
        [FoldoutGroup("Indicator")] public Transform indicatorTrans;
        GameObject indicatorInstance;

        [FoldoutGroup("Debug"), ReadOnly] public float valveAngle;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _isLinearLocked;
        [FoldoutGroup("Debug"), ReadOnly, SerializeField] private bool _isRotationLock;

        [FoldoutGroup("Setting"), SerializeField] private HVRDialMK2 valveTracker;
        [FoldoutGroup("Setting"), SerializeField] private HVRGrabbable valveGrabbable;
        [Space]
        [FoldoutGroup("Setting"), SerializeField] private float offset = 2f;

        bool isSetting;

        private void Awake()
        {
            valveStartAngle = Mathf.Clamp(valveStartAngle, valveMinAngle, valveMaxAngle);
            valveTracker.startAngle = valveStartAngle;
            valveAngle = valveStartAngle;
        }

        private void Start()
        {
            valveGrabbable.HandGrabbed.AddListener(ItemHandGrabbed);
            valveGrabbable.HandReleased.AddListener(ItemHandReleased);

            //StartCoroutine(SetMaxAngle());

            _valveMinAngle = valveMinAngle;
            _valveMaxAngle = valveMaxAngle;

        }


        private void Update()
        {
            if (isSetting) return;
            Debug.Log(valveTracker.currentAngle);
            if (valveAngle != valveTracker.currentAngle)
            {
                onAngleChanged.Invoke(valveTracker.currentAngle);
            }
            valveAngle = Mathf.Clamp(valveTracker.currentAngle, _valveMinAngle, _valveMaxAngle);

            valveMinAngle = 0;
            valveMaxAngle = Mathf.FloorToInt(valveTracker.MaximumAngle);

            foreach (var _dial in valveEvent)
            {
                if (_dial.isFulfilled)
                {
                    if (CheckOffset(_dial.customAngle) < offset) return;
                    else
                    {
                        _dial.isFulfilled = false;
                        _dial.onUnfulfilled.Invoke();
                    }
                }

                if (!_dial.isActive) return;

                var isFulfilled = false;

                if (CheckOffset(_dial.customAngle) < offset) isFulfilled = true;

                if (isFulfilled)
                {
                    _dial.onFulfilled.Invoke();
                    _dial.isFulfilled = true;
                    if (_dial.deactiveOnFullfilled)
                        _dial.isActive = false;
                }

            }
        }

        private void LateUpdate()
        {
            if (_isLinearLocked || !valveGrabbable.IsHandGrabbed)
                setIndicator(false);
            else setIndicator(true);
        }

        void setIndicator(bool b)
        {
            if (indicatorInstance)
            {
                indicatorInstance.gameObject.SetActive(b);
                int percentage = Mathf.RoundToInt((valveAngle - valveMinAngle) / (valveMaxAngle - valveMinAngle) * 100);
                if (indicatorInstance.GetComponentInChildren<TMPro.TextMeshProUGUI>(true))
                    indicatorInstance.GetComponentInChildren<TMPro.TextMeshProUGUI>(true).text = percentage.ToString() + "%";
                if (indicatorInstance.GetComponentsInChildren<Image>(true).FirstOrDefault(x => x.type == Image.Type.Filled))
                    indicatorInstance.GetComponentsInChildren<Image>(true).FirstOrDefault(x => x.type == Image.Type.Filled).fillAmount = (float)percentage * .01f;
                indicatorInstance.transform.LookAt(Camera.main.transform);
                Vector3 euler = indicatorInstance.transform.eulerAngles;
                indicatorInstance.transform.eulerAngles = new Vector3(0f, euler.y + 180f, 0f);
            }
            else if (indicatorPrefab)
            {
                indicatorInstance = Instantiate(indicatorPrefab);
                indicatorInstance.transform.parent = indicatorTrans ? indicatorTrans : transform;
                indicatorInstance.transform.localPosition = Vector3.zero;
            }
        }

        //private IEnumerator SetMaxAngle()
        //{
        //    isSetting = true;
        //    yield return new WaitUntil(() => valveLimiter.isInit);

        //    if (valveMinAngle > 0)
        //        valveMinAngle *= -1;

        //    if (valveMaxAngle < 0)
        //        valveMaxAngle *= -1;

        //    valveMinAngle = Mathf.Clamp(valveMinAngle, valveMinAngle, 0);
        //    valveMaxAngle = Mathf.Clamp(valveMaxAngle, 0, valveMaxAngle);

        //    valveLimiter.MinAngle = Mathf.RoundToInt(valveMinAngle);
        //    valveLimiter.MaxAngle = Mathf.RoundToInt(valveMaxAngle);
        //    isSetting = false;
        //}

        private float CheckOffset(float _value)
        {
            return Mathf.Abs(valveAngle - _value);
        }

        void ItemHandGrabbed(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            onHandGrabbed.Invoke();
        }

        void ItemHandReleased(HVRGrabberBase _grabber, HVRGrabbable _grabbable)
        {
            onHandReleased.Invoke();
        }

        public override bool linearLock
        {
            get { return _isLinearLocked; }
            set
            {
                if (!_linearException && Sandbox_ScenarioManager.instance.mode == ScenarioModeEnum.LINEAR)
                {
                    _isLinearLocked = value;
                    valveGrabbable.linearLock = value;
                    SetLock();
                    foreach (var outline in outlineList) outline.enabled = !value;
                }
            }
        }

        public void SetRotationLock(bool _value)
        {
            _isRotationLock = _value;
            SetLock();
        }

        private void SetLock()
        {
            bool _isLocked = _isRotationLock || _isLinearLocked;

            valveGrabbable.GetComponent<Rigidbody>().constraints = _isLocked ? RigidbodyConstraints.FreezeAll : RigidbodyConstraints.None;
            valveGrabbable.GetComponent<Rigidbody>().isKinematic = _isLocked ? true : false;

            List<HVRGrabberBase> grabberList = new List<HVRGrabberBase>(valveGrabbable.Grabbers);

            foreach (var _grabber in grabberList)
            {
                if (_grabber is HVRHandGrabber)
                    _grabber.ForceRelease();
            }
        }

        [System.Serializable]
        public class Sandbox_ValveEvent
        {
            public int customAngle;

            public UnityEvent onFulfilled;
            public UnityEvent onUnfulfilled;

            [FoldoutGroup("Setting")] public bool isActive = true;
            [FoldoutGroup("Setting")] public bool deactiveOnFullfilled = false;
            [FoldoutGroup("Setting"), ReadOnly] public bool isFulfilled = false;
        }

        bool isplaying()
        {
#if UNITY_EDITOR
            if (Application.isPlaying) return true;
            else return false;
#endif
            return false;
        }
#if UNITY_EDITOR

        //private void Reset()
        //{

        //    if (valveLimiter)
        //    {
        //        valveLimiter.MinAngle = valveMinAngle;
        //        valveLimiter.MaxAngle = valveMaxAngle;
        //    }
        //}

        //private void OnValidate()
        //{
        //    if (valveLimiter)
        //    {
        //        valveLimiter.MinAngle = valveMinAngle;
        //        valveLimiter.MaxAngle = valveMaxAngle;
        //    }
        //}
#endif
    }
}
